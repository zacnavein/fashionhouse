package com.fashionhouse.application.sales;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.application.sales.query.ProductView;
import com.fashionhouse.domain.sales.model.ProductStatus;
import com.fashionhouse.application.sales.query.ProductViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ProductQueryFacade {

    private final ProductViewRepository repository;

    public ProductView getProduct(UUID productId) {
        return repository.findById(productId);
    }

    public List<ProductView> getProducts(ProductStatus status) {
        return repository.findByStatus(status);
    }

}
