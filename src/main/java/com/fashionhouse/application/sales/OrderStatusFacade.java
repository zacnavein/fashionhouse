package com.fashionhouse.application.sales;

import com.fashionhouse.domain.sales.model.Order;
import com.fashionhouse.domain.sales.repository.OrderRepository;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class OrderStatusFacade {

    private final OrderRepository orderRepository;

    public void markAsPacked(UUID orderId) {
        Order order = orderRepository.getById(orderId);
        order.packed();
        orderRepository.save(order);
    }

    public void markAsSent(UUID orderId) {
        Order order = orderRepository.getById(orderId);
        order.sent();
        orderRepository.save(order);
    }

    public void markAsReturned(UUID orderId) {
        Order order = orderRepository.getById(orderId);
        order.returned();
        orderRepository.save(order);
    }

}
