package com.fashionhouse.application.sales;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import com.fashionhouse.application.sales.command.ChangeOrderDeliveryCommand;
import com.fashionhouse.application.sales.command.ChangeInvoiceCommand;
import com.fashionhouse.application.sales.command.SubmitOrderCommand;
import com.fashionhouse.domain.sales.event.OrderEventFactory;
import com.fashionhouse.domain.sales.event.RefundOrderEvent;
import com.fashionhouse.domain.sales.model.Delivery;
import com.fashionhouse.domain.sales.model.Invoice;
import com.fashionhouse.domain.sales.model.Order;
import com.fashionhouse.domain.sales.model.OrderFactory;
import com.fashionhouse.domain.sales.repository.OrderRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class OrderFacade {

    private final OrderFactory orderFactory;
    private final OrderRepository orderRepository;
    private final OrderEventFactory orderEventFactory;
    private final EventProducer eventPublisher;

    public void submitOrder(User user, SubmitOrderCommand command) {
        Order order = orderFactory.createOrderToSubmit(user.getId(), command);
        orderRepository.save(order);
        eventPublisher.publishEvent(orderEventFactory.createSubmitOrderEvent(order));
    }

    public void finishOrder(User user, UUID orderId) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        order.delivered();
        orderRepository.save(order);
    }

    public void changeOrderDelivery(User user, UUID orderId, ChangeOrderDeliveryCommand command) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        Delivery delivery = orderFactory.createDelivery(command);
        order.changeDelivery(delivery);
        orderRepository.save(order);
        eventPublisher.publishEvent(orderEventFactory.createOrderDeliveryChangeEvent(order));
    }

    public void changeOrderInvoice(User user, UUID orderId, ChangeInvoiceCommand command) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        Invoice invoice = orderFactory.createInvoice(command);
        order.changeInvoice(invoice);
        orderRepository.save(order);
        eventPublisher.publishEvent(orderEventFactory.createOrderInvoiceChangeEvent(order));
    }

    public void refundOrder(User user, UUID orderId) {
        Order order = orderRepository.getByIdAndBuyerId(orderId, user.getId());
        refund(user, order);
    }

    public void refundOrder(User user, UUID customerId, UUID orderId) {
        user.authorize(Permission.ORDER_MANAGEMENT);
        Order order = orderRepository.getByIdAndBuyerId(orderId, customerId);
        refund(user, order);
    }

    private void refund(User user, Order order) {
        order.refund();
        orderRepository.save(order);
        eventPublisher.publishEvent(new RefundOrderEvent(order.getId(), user.getId()));
    }

}
