package com.fashionhouse.application.sales.command;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerAddressCommand {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
