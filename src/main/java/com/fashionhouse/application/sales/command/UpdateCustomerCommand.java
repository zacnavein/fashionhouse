package com.fashionhouse.application.sales.command;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateCustomerCommand {

    private String email;
    private String phone;
    private String firstName;
    private String lastName;

}
