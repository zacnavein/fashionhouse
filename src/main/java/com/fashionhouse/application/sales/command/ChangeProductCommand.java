package com.fashionhouse.application.sales.command;

import com.fashionhouse.infrastructure.validator.annotations.Currency;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ChangeProductCommand {

    @NotNull
    @Currency
    private Double price;
    @NotNull
    private String name;
    private String description;

}
