package com.fashionhouse.application.sales.command;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class CreateProductCommand {

    private UUID productId;
    private String name;
    private String description;

}
