package com.fashionhouse.application.sales.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecipientCommand {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
