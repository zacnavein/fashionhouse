package com.fashionhouse.application.sales.command;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubmitOrderCommand {

    @NotNull
    @NotEmpty
    private List<OrderProductCommand> products;
    @NotNull
    private RecipientCommand recipient;
    @NotNull
    private OrderDeliveryCommand delivery;
    private InvoiceCommand invoice;

}
