package com.fashionhouse.application.sales.command;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ChangeCustomerEmailCommand {

    private UUID customerId;
    private String newEmail;

}
