package com.fashionhouse.application.sales;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.sales.query.OrderView;
import com.fashionhouse.application.sales.query.OrderViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class OrderQueryFacade {

    private final OrderViewRepository repository;

    public OrderView getMyOrder(User user, UUID orderId) {
        return repository.findByIdAndBuyerId(orderId, user.getId());
    }

    public List<OrderView> getMyOrders(User user) {
        return repository.findByBuyerId(user.getId());
    }

    public OrderView getOrder(User user, UUID orderId) {
        user.authorize(Permission.SHOW_ORDERS);
        return repository.findById(orderId);
    }

    public List<OrderView> getOrders(User user) {
        user.authorize(Permission.SHOW_ORDERS);
        return repository.findAll();
    }

}
