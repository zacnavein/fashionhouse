package com.fashionhouse.application.sales;

import com.fashionhouse.application.sales.command.CreateProductCommand;
import com.fashionhouse.application.sales.command.ProductStockUpdateCommand;
import com.fashionhouse.domain.sales.model.ProductFactory;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.sales.command.ChangeProductCommand;
import com.fashionhouse.domain.sales.model.Product;
import com.fashionhouse.domain.sales.model.ProductDescription;
import com.fashionhouse.domain.sales.repository.ProductRepository;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationService
@AllArgsConstructor
public class ProductManagementFacade {

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;

    public void createProduct(CreateProductCommand command) {
        productRepository.save(productFactory.createProduct(command));
    }

    public void changeProduct(User user, UUID productId, ChangeProductCommand command) {
        user.authorize(Permission.PRODUCT_MANAGEMENT);
        Product product = productRepository.getById(productId);
        ProductDescription description = productFactory.createProductDescription(command);
        product.changeDescription(description);
        productRepository.save(product);
    }

    public void availableProduct(User user, UUID productId) {
        user.authorize(Permission.PRODUCT_MANAGEMENT);
        Product product = productRepository.getById(productId);
        product.available();
        productRepository.save(product);
    }

    public void unavailableProduct(User user, UUID productId) {
        user.authorize(Permission.PRODUCT_MANAGEMENT);
        Product product = productRepository.getById(productId);
        product.unavailable();
        productRepository.save(product);
    }

    public void updateProductStock(List<ProductStockUpdateCommand> commands) {
        List<UUID> productIds = commands.stream().map(ProductStockUpdateCommand::getProductId).collect(Collectors.toList());
        Map<UUID, Product> products = productRepository.getProductsById(productIds);
        commands.forEach(command -> products.get(command.getProductId()).updateStock(command.getStock()));
        productRepository.saveAll(products);
    }

}
