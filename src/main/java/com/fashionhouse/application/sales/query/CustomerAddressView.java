package com.fashionhouse.application.sales.query;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class CustomerAddressView {

    private UUID id;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
