package com.fashionhouse.application.sales.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderView {

    private UUID id;
    private UUID buyerId;
    private DeliveryView delivery;
    private RecipientView recipient;
    private InvoiceView invoice;
    private List<OrderProductView> products;

}
