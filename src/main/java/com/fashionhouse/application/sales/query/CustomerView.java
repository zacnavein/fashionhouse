package com.fashionhouse.application.sales.query;

import lombok.Builder;
import lombok.Data;
import java.util.List;
import java.util.UUID;

@Data
@Builder
public class CustomerView {

    private UUID id;
    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private List<CustomerAddressView> addresses;

}
