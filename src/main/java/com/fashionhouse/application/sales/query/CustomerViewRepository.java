package com.fashionhouse.application.sales.query;

import com.fashionhouse.domain.sales.model.Customer;
import com.fashionhouse.domain.sales.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class CustomerViewRepository {

    private final CustomerRepository repository;

    public CustomerView findById(UUID id) { // should return dto directly from database
        Customer customer = repository.getById(id);
        return CustomerView.builder()
                .id(customer.getId())
                .firstName(customer.getCustomer().getFirstName())
                .lastName(customer.getCustomer().getLastName())
                .email(customer.getCustomer().getEmail())
                .email(customer.getCustomer().getPhone())
                .build();
    }

    public List<CustomerView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(customer -> CustomerView.builder()
                        .id(customer.getId())
                        .firstName(customer.getCustomer().getFirstName())
                        .lastName(customer.getCustomer().getLastName())
                        .email(customer.getCustomer().getEmail())
                        .email(customer.getCustomer().getPhone())
                        .build())
                .collect(Collectors.toList());
    }

}
