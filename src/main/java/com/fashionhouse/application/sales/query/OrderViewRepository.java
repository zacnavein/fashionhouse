package com.fashionhouse.application.sales.query;

import com.fashionhouse.domain.sales.model.*;
import com.fashionhouse.domain.sales.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class OrderViewRepository {

    private OrderRepository repository;

    public OrderView findById(UUID id) { // should return dto directly from database
        return map(repository.getById(id));
    }

    public OrderView findByIdAndBuyerId(UUID id, UUID buyerId) { // should return dto directly from database
        return map(repository.getByIdAndBuyerId(id, buyerId));
    }

    public List<OrderView> findByBuyerId(UUID buyerId) { // should return dto directly from database
        return repository.findByBuyerId(buyerId).stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public List<OrderView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private OrderView map(Order order) {
        return OrderView.builder()
                .id(order.getId())
                .buyerId(order.getBuyerId())
                .delivery(map(order.getDelivery()))
                .invoice(map(order.getInvoice()))
                .recipient(map(order.getRecipient()))
                .products(map(order.getOrderProducts()))
                .build();
    }

    private InvoiceView map(Invoice invoice) {
        return InvoiceView.builder()
                .nip(invoice.getNip())
                .name(invoice.getName())
                .address(invoice.getAddress())
                .date(invoice.getDate())
                .build();
    }

    private DeliveryView map(Delivery delivery) {
        return DeliveryView.builder()
                .postcode(delivery.getPostcode())
                .city(delivery.getCity())
                .postcode(delivery.getPostcode())
                .description(delivery.getDescription())
                .build();
    }

    private RecipientView map(Recipient recipient) {
        return RecipientView.builder()
                .firstName(recipient.getFirstName())
                .lastName(recipient.getLastName())
                .email(recipient.getEmail())
                .phone(recipient.getPhone())
                .build();
    }

    private List<OrderProductView> map(Set<OrderProduct> products) {
        return products.stream()
                .map(product ->
                        OrderProductView.builder()
                                .productId(product.getId())
                                .name(product.getProduct().getName())
                                .amount(product.getAmount())
                                .price(product.getProduct().getPrice())
                                .description(product.getProduct().getDescription())
                                .build())
                .collect(Collectors.toList());
    }

}
