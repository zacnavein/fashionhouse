package com.fashionhouse.application.sales.query;

import com.fashionhouse.domain.sales.model.Product;
import com.fashionhouse.domain.sales.model.ProductStatus;
import com.fashionhouse.domain.sales.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class ProductViewRepository {

    private ProductRepository repository;

    public ProductView findById(UUID id) { // should return dto directly from database
        return map(repository.getById(id));
    }

    public List<ProductView> findByStatus(ProductStatus status) { // should return dto directly from database
        return repository.findByStatus(status).stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public List<ProductView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private ProductView map(Product product) {
        return ProductView.builder()
                .id(product.getId())
                .stock(product.getStock())
                .price(product.getDescription().getPrice())
                .name(product.getDescription().getName())
                .description(product.getDescription().getDescription())
                .build();
    }

}
