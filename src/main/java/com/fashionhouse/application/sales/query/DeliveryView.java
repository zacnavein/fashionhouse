package com.fashionhouse.application.sales.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryView {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
