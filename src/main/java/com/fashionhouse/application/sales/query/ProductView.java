package com.fashionhouse.application.sales.query;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ProductView {

    private UUID id;
    private Double price;
    private String name;
    private String description;
    private Integer stock;

}
