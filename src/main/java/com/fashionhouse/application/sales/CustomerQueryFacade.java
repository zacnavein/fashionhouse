package com.fashionhouse.application.sales;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.sales.query.CustomerView;
import com.fashionhouse.application.sales.query.CustomerViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class CustomerQueryFacade {

    private final CustomerViewRepository repository;
    public CustomerView getMyCustomer(User user) {
        return repository.findById(user.getId());
    }

    public CustomerView getCustomer(User user, UUID customerId) {
        user.authorize(Permission.SHOW_CUSTOMERS);
        return repository.findById(customerId);
    }

    public List<CustomerView> getCustomers(User user) {
        user.authorize(Permission.SHOW_CUSTOMERS);
        return repository.findAll();
    }

}
