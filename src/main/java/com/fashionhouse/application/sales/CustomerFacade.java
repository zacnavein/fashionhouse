package com.fashionhouse.application.sales;

import com.fashionhouse.application.sales.command.ChangeCustomerEmailCommand;
import com.fashionhouse.application.sales.command.CreateCustomerCommand;
import com.fashionhouse.domain.sales.model.*;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.sales.command.CustomerAddressCommand;
import com.fashionhouse.application.sales.command.UpdateCustomerCommand;
import com.fashionhouse.domain.sales.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class CustomerFacade {

    private final CustomerRepository customerRepository;
    private final CustomerFactory customerFactory;
    private final AddressFactory addressFactory;

    public void createCustomer(CreateCustomerCommand command) {
        Customer customer = customerFactory.createCustomer(command);
        customerRepository.save(customer);
    }

    public void updateCustomer(User user, UpdateCustomerCommand command) {
        Customer customer = customerRepository.getById(user.getId());
        CustomerDetails details = customerFactory.createCustomerDetails(command);
        customer.changeDetails(details);
        customerRepository.save(customer);
    }

    public void changeCustomerEmail(ChangeCustomerEmailCommand command) {
        Customer customer = customerRepository.getById(command.getCustomerId());
        customer.changeEmail(command.getNewEmail());
        customerRepository.save(customer);
    }

    public void createAddress(User user, CustomerAddressCommand command) {
        Customer customer = customerRepository.getById(user.getId());
        Address address = addressFactory.create(command);
        customer.addAddress(address);
        customerRepository.save(customer);
    }

    public void removeAddress(User user, UUID addressId) {
        Customer customer = customerRepository.getById(user.getId());
        customer.removeAddress(addressId);
        customerRepository.save(customer);
    }

}
