package com.fashionhouse.application.design;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import com.fashionhouse.domain.design.event.DesignEventFactory;
import com.fashionhouse.domain.design.model.Design;
import com.fashionhouse.domain.design.repository.DesignRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class DesignVerifyFacade {

    private final DesignRepository designRepository;
    private final DesignEventFactory designEventFactory;
    private final EventProducer eventPublisher;

    public void approveDesign(User user, UUID designId) {
        user.authorize(Permission.VERIFY_DESIGN);
        Design design = designRepository.getById(designId);
        design.approve(user.getId());
        designRepository.save(design);
        eventPublisher.publishEvent(designEventFactory.createDesignApprovedEvent(design));
    }

    public void declineDesign(User user, UUID designId) {
        user.authorize(Permission.VERIFY_DESIGN);
        Design design = designRepository.getById(designId);
        design.decline(user.getId());
        designRepository.save(design);
    }

}
