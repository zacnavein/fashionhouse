package com.fashionhouse.application.design;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.design.query.DesignView;
import com.fashionhouse.application.design.query.DesignViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class DesignQueryFacade {

    private final DesignViewRepository designResultRepository;

    public DesignView getDesign(User user, UUID designId) {
        user.authorize(Permission.SHOW_DESIGN);
        return designResultRepository.findById(designId);
    }

    public List<DesignView> getDesigns(User user) {
        user.authorize(Permission.SHOW_DESIGN);
        return designResultRepository.findAll();
    }

}
