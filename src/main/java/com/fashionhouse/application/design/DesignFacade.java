package com.fashionhouse.application.design;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.design.command.DesignCommand;
import com.fashionhouse.domain.design.model.Design;
import com.fashionhouse.domain.design.model.DesignFactory;
import com.fashionhouse.domain.design.repository.DesignRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class DesignFacade {

    private final DesignRepository designRepository;
    private final DesignFactory designFactory;

    public void createDesign(User user, DesignCommand command) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designFactory.createDesign(user.getId(), command);
        designRepository.save(design);
    }

    public void updateDesign(User user, UUID designId, DesignCommand command) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designRepository.getByIdAndDesignerId(designId, user.getId());
        design.changeDesign(designFactory.createDesignDetails(command));
    }

    public void finishDesign(User user, UUID designId) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designRepository.getByIdAndDesignerId(designId, user.getId());
        design.finish();
    }

    public void cancelDesign(User user, UUID designId) {
        user.authorize(Permission.MAKE_DESIGN);
        Design design = designRepository.getById(designId);
        design.cancel();
    }

}
