package com.fashionhouse.application.design.query;

import com.fashionhouse.domain.design.model.DesignStatus;
import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class DesignView {

    private UUID id;
    private String name;
    private String type;
    private String description;
    private String url;
    private DesignStatus status;

}
