package com.fashionhouse.application.design.query;

import com.fashionhouse.domain.design.model.Design;
import com.fashionhouse.domain.design.repository.DesignRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class DesignViewRepository {

    private final DesignRepository repository;
    public DesignView findById(UUID id) { // should return dto directly from database
        return map(repository.getById(id));
    }

    public List<DesignView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private DesignView map(Design design) {
        return DesignView.builder()
                .id(design.getId())
                .status(design.getStatus())
                .name(design.getDesign().getName())
                .description(design.getDesign().getDescription())
                .type(design.getDesign().getType())
                .build();
    }

}
