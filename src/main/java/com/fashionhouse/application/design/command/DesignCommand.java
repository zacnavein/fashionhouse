package com.fashionhouse.application.design.command;

import com.fashionhouse.infrastructure.validator.annotations.URL;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DesignCommand {

    @NotNull
    private String name;
    @NotNull
    private String type;
    @NotNull
    private String description;
    @URL
    @NotNull
    private String url;

}
