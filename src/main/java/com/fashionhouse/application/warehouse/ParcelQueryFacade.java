package com.fashionhouse.application.warehouse;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.warehouse.query.ParcelView;
import com.fashionhouse.application.warehouse.query.ParcelViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ParcelQueryFacade {

    private final ParcelViewRepository repository;

    public List<ParcelView> getParcels(User user) {
        user.authorize(Permission.SHOW_PARCEL);
        return repository.findAll();
    }

    public ParcelView getParcel(User user, UUID parcelId) {
        user.authorize(Permission.SHOW_PARCEL);
        return repository.findById(parcelId);
    }

}
