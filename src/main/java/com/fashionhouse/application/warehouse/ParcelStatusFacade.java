package com.fashionhouse.application.warehouse;

import com.fashionhouse.domain.warehouse.model.ParcelFactory;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import com.fashionhouse.application.warehouse.command.ParcelMeasurementCommand;
import com.fashionhouse.domain.warehouse.event.ParcelEventFactory;
import com.fashionhouse.domain.warehouse.model.Measurement;
import com.fashionhouse.domain.warehouse.model.Parcel;
import com.fashionhouse.domain.warehouse.repository.ParcelRepository;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ParcelStatusFacade {

    private final ParcelRepository parcelRepository;
    private final ParcelFactory parcelFactory;
    private final ParcelEventFactory parcelEventFactory;
    private final EventProducer eventPublisher;

    public void packedParcel(User user, UUID parcelId, ParcelMeasurementCommand command) {
        user.authorize(Permission.PARCEL_SENDER);
        Parcel parcel = parcelRepository.getById(parcelId);
        Measurement measurement = parcelFactory.createMeasurement(command);
        parcel.packed(measurement);
        parcelRepository.save(parcel);
        eventPublisher.publishEvent(parcelEventFactory.createParcelPackedEvent(parcel));
    }

    public void sendParcel(User user, UUID parcelId) {
        user.authorize(Permission.PARCEL_SENDER);
        Parcel parcel = parcelRepository.getById(parcelId);
        parcel.sent();
        parcelRepository.save(parcel);
        eventPublisher.publishEvent(parcelEventFactory.createParcelSentEvent(parcel));
    }

    public void returnedParcel(User user, UUID parcelId) {
        user.authorize(Permission.PARCEL_SENDER);
        Parcel parcel = parcelRepository.getById(parcelId);
        parcel.returned();
        parcelRepository.save(parcel);
        eventPublisher.publishEvent(parcelEventFactory.createParcelReturnedEvent(parcel));
    }

    public void deliveredParcel(User user, UUID parcelId) {
        Parcel parcel = parcelRepository.getById(parcelId);
        if (!parcel.isOwner(user.getId())) {
            user.authorize(Permission.ORDER_MANAGEMENT);
        }
        parcel.delivered();
        parcelRepository.save(parcel);
    }

}
