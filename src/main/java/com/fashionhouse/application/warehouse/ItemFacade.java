package com.fashionhouse.application.warehouse;

import com.fashionhouse.application.warehouse.command.*;
import com.fashionhouse.domain.warehouse.event.ItemEventFactory;
import com.fashionhouse.domain.warehouse.model.*;
import com.fashionhouse.domain.warehouse.repository.ItemRepository;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import lombok.AllArgsConstructor;

import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ItemFacade {

    private final ItemRepository itemRepository;
    private final ItemFactory itemFactory;
    private final ItemEventFactory itemEventFactory;
    private final EventProducer eventPublisher;

    public void createItem(CreateItemCommand command) {
        itemRepository.save(create(command));
    }

    public void updateItemLocation(User user, UUID itemId, ItemLocationCommand command) {
        user.authorize(Permission.ITEM_MANAGER);
        Item item = itemRepository.getById(itemId);
        Location location = itemFactory.createLocation(command);
        item.changeLocation(location);
        itemRepository.save(item);
    }

    public void updateItemDescription(User user, UUID itemId, ItemDescriptionCommand request) {
        user.authorize(Permission.ITEM_MANAGER);
        Item item = itemRepository.getById(itemId);
        ItemDescription description = itemFactory.createItemDescription(request);
        item.changeDescription(description);
        itemRepository.save(item);
    }

    public void updateItemMeasurement(User user, UUID itemId, ItemMeasurementCommand request) {
        user.authorize(Permission.ITEM_MANAGER);
        Item item = itemRepository.getById(itemId);
        Measurement description = itemFactory.createMeasurement(request);
        item.changeMeasurement(description);
        itemRepository.save(item);
    }

    public void itemSupply(User user, UUID itemId, Integer amount) {
        user.authorize(Permission.ITEM_SUPPLIER);
        Item item = itemRepository.getById(itemId);
        item.supply(amount);
        itemRepository.save(item);
        eventPublisher.publishEvent(itemEventFactory.createSupplyEvent(item, amount));
    }

    public void itemDraw(User user, UUID itemId, Integer amount) {
        user.authorize(Permission.ITEM_SUPPLIER);
        Item item = itemRepository.getById(itemId);
        item.draw(amount);
        itemRepository.save(item);
        eventPublisher.publishEvent(itemEventFactory.createDrawEvent(item, amount));
    }

    private Item create(CreateItemCommand command) {
        return Item.builder()
                .id(command.getItemId())
                .description(createDescription(command))
                .build();
    }

    private ItemDescription createDescription(CreateItemCommand command) {
        return ItemDescription.builder()
                .name(command.getName())
                .comment(command.getDescription())
                .build();
    }

}
