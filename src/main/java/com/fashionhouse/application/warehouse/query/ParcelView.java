package com.fashionhouse.application.warehouse.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParcelView {

    private UUID id;
    private UUID orderId;
    private UUID recipientId;
    private ParcelDestinationView destination;
    private ParcelMeasurementView measurement;
    private ParcelRecipientView recipient;
    private List<ParcelItemView> items;

}
