package com.fashionhouse.application.warehouse.query;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemLocationView {

    private String sector;
    private String shelf;
    private String bucket;

}
