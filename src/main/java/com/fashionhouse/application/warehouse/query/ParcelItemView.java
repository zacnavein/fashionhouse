package com.fashionhouse.application.warehouse.query;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ParcelItemView {

    private UUID id;
    private UUID itemId;
    private Integer amount;
    private String name;

}
