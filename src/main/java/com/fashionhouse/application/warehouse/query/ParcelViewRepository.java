package com.fashionhouse.application.warehouse.query;

import com.fashionhouse.domain.warehouse.model.*;
import com.fashionhouse.domain.warehouse.repository.ParcelRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class ParcelViewRepository {

    private final ParcelRepository repository;

    public ParcelView findById(UUID id) { // should return dto directly from database
        return map(repository.getById(id));
    }

    public List<ParcelView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private ParcelView map(Parcel parcel) {
        return ParcelView.builder()
                .id(parcel.getId())
                .orderId(parcel.getOrderId())
                .destination(map(parcel.getDestination()))
                .recipient(map(parcel.getRecipient()))
                .measurement(map(parcel.getMeasurement()))
                .items(map(parcel.getParcelItems()))
                .build();
    }

    private ParcelDestinationView map(Destination destination) {
        return ParcelDestinationView.builder()
                .postcode(destination.getPostcode())
                .city(destination.getCity())
                .postcode(destination.getPostcode())
                .description(destination.getDescription())
                .build();
    }

    private ParcelRecipientView map(Recipient recipient) {
        return ParcelRecipientView.builder()
                .firstName(recipient.getFirstName())
                .lastName(recipient.getLastName())
                .email(recipient.getEmail())
                .phone(recipient.getPhone())
                .build();
    }

    private ParcelMeasurementView map(Measurement measurement) {
        return ParcelMeasurementView.builder()
                .height(measurement.getHeight())
                .width(measurement.getWidth())
                .length(measurement.getLength())
                .weight(measurement.getWeight())
                .build();
    }

    private List<ParcelItemView> map(Set<ParcelItem> items) {
        return items.stream()
                .map(item ->
                        ParcelItemView.builder()
                                .id(item.getId())
                                .itemId(item.getItemId())
                                .amount(item.getAmount())
                                .name(item.getItem().getName())
                                .build())
                .collect(Collectors.toList());
    }

}
