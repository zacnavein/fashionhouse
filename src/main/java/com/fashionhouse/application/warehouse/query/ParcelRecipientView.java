package com.fashionhouse.application.warehouse.query;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParcelRecipientView {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
