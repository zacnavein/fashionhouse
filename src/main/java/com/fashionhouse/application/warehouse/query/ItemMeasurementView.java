package com.fashionhouse.application.warehouse.query;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemMeasurementView {

    private Double length;
    private Double width;
    private Double height;
    private Double weight;

}
