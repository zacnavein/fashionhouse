package com.fashionhouse.application.warehouse.query;

import com.fashionhouse.domain.warehouse.model.Item;
import com.fashionhouse.domain.warehouse.repository.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class ItemViewRepository {

    private final ItemRepository repository;

    public ItemView findById(UUID id) { // should return dto directly from database
        return map(repository.getById(id));
    }

    public List<ItemView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private ItemView map(Item item) {
        return ItemView.builder()
                .id(item.getId())
                .stock(item.getStock())
                .name(item.getDescription().getName())
                .comment(item.getDescription().getComment())
                .location(
                        ItemLocationView.builder()
                                .sector(item.getLocation().getSector())
                                .shelf(item.getLocation().getShelf())
                                .bucket(item.getLocation().getBucket())
                                .build()
                )
                .measurement(
                        ItemMeasurementView.builder()
                                .height(item.getMeasurement().getHeight())
                                .width(item.getMeasurement().getWidth())
                                .length(item.getMeasurement().getLength())
                                .weight(item.getMeasurement().getWeight())
                                .build()
                )
                .build();
    }

}
