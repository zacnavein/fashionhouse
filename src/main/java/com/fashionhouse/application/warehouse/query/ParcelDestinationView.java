package com.fashionhouse.application.warehouse.query;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParcelDestinationView {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
