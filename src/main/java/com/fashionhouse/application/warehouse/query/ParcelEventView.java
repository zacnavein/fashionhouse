package com.fashionhouse.application.warehouse.query;

import com.fashionhouse.domain.warehouse.model.ParcelStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParcelEventView {

    private ParcelStatus status;
    private LocalDateTime date;

}
