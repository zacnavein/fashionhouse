package com.fashionhouse.application.warehouse.query;

import lombok.Builder;
import lombok.Data;
import java.util.UUID;

@Data
@Builder
public class ItemView {

    private UUID id;
    private String name;
    private String comment;
    private Integer stock;
    private ItemLocationView location;
    private ItemMeasurementView measurement;

}
