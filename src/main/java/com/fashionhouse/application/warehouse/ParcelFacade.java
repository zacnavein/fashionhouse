package com.fashionhouse.application.warehouse;

import com.fashionhouse.application.warehouse.command.ChangeParcelInvoiceCommand;
import com.fashionhouse.application.warehouse.command.CreateParcelDemandCommand;
import com.fashionhouse.application.warehouse.command.ChangeParcelDeliveryCommand;
import com.fashionhouse.domain.warehouse.event.ItemEventFactory;
import com.fashionhouse.domain.warehouse.event.UnrealizableParcelEvent;
import com.fashionhouse.domain.warehouse.ex.ItemOutOfStockException;
import com.fashionhouse.domain.warehouse.model.Item;
import com.fashionhouse.domain.warehouse.model.Parcel;
import com.fashionhouse.domain.warehouse.model.ParcelFactory;
import com.fashionhouse.domain.warehouse.repository.ItemRepository;
import com.fashionhouse.domain.warehouse.repository.ParcelRepository;
import com.fashionhouse.domain.warehouse.service.StockService;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ParcelFacade {

    private final ParcelFactory parcelFactory;
    private final ParcelRepository parcelRepository;
    private final ItemRepository itemRepository;
    private final StockService stockService;
    private final ItemEventFactory itemEventFactory;
    private final EventProducer eventProducer;

    public void createParcelDemand(CreateParcelDemandCommand command) {
        try {
            Map<UUID, Item> items = itemRepository.getItemsById(command.getItemIds());
            Parcel parcel = parcelFactory.createParcel(command);
            stockService.drawItemsToParcel(parcel, items);
            parcelRepository.save(parcel);
            itemRepository.saveAll(items);
            eventProducer.publishEvent(itemEventFactory.createDrawEvent(parcel, items));
        } catch (ItemOutOfStockException e) {
            eventProducer.publishEvent(new UnrealizableParcelEvent(command.getOrderId()));
            throw e;
        }
    }

    public void cancelParcelDelivery(User user, UUID orderId) {
        Parcel parcel = parcelRepository.getByOrderId(orderId);
        if (!parcel.isOwner(user.getId())) {
            user.authorize(Permission.ORDER_MANAGEMENT);
        }
        parcel.cancel();
        parcelRepository.save(parcel);
        Map<UUID, Item> items = itemRepository.getItemsById(parcel.getParcelItemIds());
        stockService.supplyItemsFromParcel(parcel, itemRepository.getItemsById(parcel.getParcelItemIds()));
        itemRepository.saveAll(items);
        eventProducer.publishEvent(itemEventFactory.createSupplyEvent(parcel, items));
    }

    public void changeParcelDelivery(ChangeParcelDeliveryCommand command) {
        Parcel parcel = parcelRepository.getById(command.getOrderId());
        parcel.changeDestination(parcelFactory.createDestination(command));
        parcelRepository.save(parcel);
    }

    public void changeParcelInvoice(ChangeParcelInvoiceCommand command) {
        Parcel parcel = parcelRepository.getById(command.getOrderId());
        parcel.changeInvoice(parcelFactory.createInvoice(command));
        parcelRepository.save(parcel);
    }

}
