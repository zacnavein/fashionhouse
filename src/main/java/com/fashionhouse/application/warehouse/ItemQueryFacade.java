package com.fashionhouse.application.warehouse;

import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.warehouse.query.ItemView;
import com.fashionhouse.application.warehouse.query.ItemViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class ItemQueryFacade {

    private final ItemViewRepository repository;

    public List<ItemView> getItems(User user) {
        user.authorize(Permission.SHOW_ITEM);
        return repository.findAll();
    }

    public ItemView getItem(User user, UUID itemId) {
        user.authorize(Permission.SHOW_ITEM);
        return repository.findById(itemId);
    }

}
