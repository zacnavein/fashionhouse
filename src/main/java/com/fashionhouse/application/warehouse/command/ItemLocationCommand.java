package com.fashionhouse.application.warehouse.command;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemLocationCommand {

    @NotNull
    private String sector;
    @NotNull
    private String shelf;
    @NotNull
    private String bucket;

}
