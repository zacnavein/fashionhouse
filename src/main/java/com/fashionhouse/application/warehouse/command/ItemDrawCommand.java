package com.fashionhouse.application.warehouse.command;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class ItemDrawCommand {

    private List<ItemStockCommand> stock;

    public List<UUID> getItemIds() {
        return stock.stream()
                .map(ItemStockCommand::getItemId)
                .collect(Collectors.toList());
    }

}
