package com.fashionhouse.application.warehouse.command;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
public class ChangeParcelInvoiceCommand {

    private UUID orderId;
    private String name;
    private String nip;
    private String address;
    private LocalDateTime date;

}
