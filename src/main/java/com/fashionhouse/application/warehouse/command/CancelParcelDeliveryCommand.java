package com.fashionhouse.application.warehouse.command;

import lombok.Data;
import java.util.UUID;

@Data
public class CancelParcelDeliveryCommand {

    private UUID orderId;
    private UUID requesterId;

}
