package com.fashionhouse.application.warehouse.command;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ItemDescriptionCommand {

    @NotNull
    private String name;
    private String comment;

}
