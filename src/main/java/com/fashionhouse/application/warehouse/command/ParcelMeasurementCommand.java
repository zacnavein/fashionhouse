package com.fashionhouse.application.warehouse.command;

import com.fashionhouse.infrastructure.validator.annotations.PositiveNumber;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParcelMeasurementCommand {

    @NotNull
    @PositiveNumber
    private Double length;
    @NotNull
    @PositiveNumber
    private Double width;
    @NotNull
    @PositiveNumber
    private Double height;
    @NotNull
    @PositiveNumber
    private Double weight;

}
