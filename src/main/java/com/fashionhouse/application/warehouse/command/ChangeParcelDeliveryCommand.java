package com.fashionhouse.application.warehouse.command;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
@Builder
public class ChangeParcelDeliveryCommand {

    private UUID orderId;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
