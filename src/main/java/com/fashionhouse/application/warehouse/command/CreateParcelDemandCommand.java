package com.fashionhouse.application.warehouse.command;

import com.fashionhouse.domain.warehouse.model.Destination;
import com.fashionhouse.domain.warehouse.model.Invoice;
import com.fashionhouse.domain.warehouse.model.Recipient;
import com.fashionhouse.domain.warehouse.model.ParcelItem;
import lombok.Builder;
import lombok.Data;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@Builder
public class CreateParcelDemandCommand {

    private UUID orderId;
    private UUID buyerId;
    private Recipient recipient;
    private Destination destination;
    private Invoice invoice;
    private List<ParcelItem> parcelItems;

    public List<UUID> getItemIds() {
        return parcelItems.stream().map(ParcelItem::getItemId).collect(Collectors.toList());
    }

}
