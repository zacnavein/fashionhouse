package com.fashionhouse.application.warehouse.command;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ItemStockCommand {

    private UUID itemId;
    private Integer amount;

}
