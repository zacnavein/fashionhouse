package com.fashionhouse.application.warehouse.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.List;

@Data
@AllArgsConstructor
public class ItemSupplyCommand {

    private List<ItemStockCommand> stock;

}
