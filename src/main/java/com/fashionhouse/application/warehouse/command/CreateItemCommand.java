package com.fashionhouse.application.warehouse.command;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class CreateItemCommand {

    private UUID itemId;
    private String name;
    private String description;

}
