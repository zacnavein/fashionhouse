package com.fashionhouse.application.account;

import com.fashionhouse.application.account.query.AccountView;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.application.account.query.AccountViewRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class AccountQueryFacade {

    private final AccountViewRepository repository;

    public AccountView getMyAccount(User user) {
        return repository.findById(user.getId());
    }

    public AccountView getAccount(User user, UUID accountId) {
        user.authorize(Permission.SHOW_ACCOUNT);
        return repository.findById(accountId);
    }

    public List<AccountView> getAccounts(User user) {
        user.authorize(Permission.SHOW_ACCOUNT);
        return repository.findAll();
    }

}
