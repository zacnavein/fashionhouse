package com.fashionhouse.application.account.command;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = "confirmingPassword")
@NoArgsConstructor
@AllArgsConstructor
public class ChangeEmailCommand {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String confirmingPassword;

}
