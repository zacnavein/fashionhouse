package com.fashionhouse.application.account.command;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = {"confirmingPassword", "newPassword"})
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordCommand {

    @NotNull
    private String newPassword;
    @NotNull
    private String confirmingPassword;

}
