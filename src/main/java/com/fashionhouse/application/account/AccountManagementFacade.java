package com.fashionhouse.application.account;

import com.fashionhouse.application.account.command.ChangeEmailCommand;
import com.fashionhouse.application.account.command.ChangePasswordCommand;
import com.fashionhouse.domain.account.event.AccountEmailChangedEvent;
import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.domain.account.policy.EmailPolicy;
import com.fashionhouse.domain.account.policy.PasswordPolicy;
import com.fashionhouse.domain.account.repository.AccountRepository;
import com.fashionhouse.domain.account.service.AuthenticationService;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.authentication.model.Permission;
import com.fashionhouse.infrastructure.authentication.model.Role;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import lombok.AllArgsConstructor;
import java.util.UUID;

@ApplicationService
@AllArgsConstructor
public class AccountManagementFacade {

    private final AccountRepository accountRepository;
    private final PasswordPolicy passwordPolicy;
    private final EmailPolicy emailPolicy;
    private final AuthenticationService authenticationService;
    private final EventProducer eventPublisher;

    public void changeMyEmail(User user, ChangeEmailCommand command) {
        authenticationService.authenticate(user.getUsername(), command.getConfirmingPassword());
        Account account = accountRepository.getById(user.getId());
        account.changeEmail(command.getEmail(), emailPolicy);
        accountRepository.save(account);
        eventPublisher.publishEvent(new AccountEmailChangedEvent(account.getId(), account.getEmail()));
    }

    public void changeMyPassword(User user, ChangePasswordCommand command) {
        authenticationService.authenticate(user.getUsername(), command.getConfirmingPassword());
        Account account = accountRepository.getById(user.getId());
        account.changePassword(command.getNewPassword(), passwordPolicy);
        accountRepository.save(account);
    }

    public void changeEmail(User user, UUID accountId, ChangeEmailCommand command) {
        user.authorize(Permission.CHANGE_EMAIL);
        authenticationService.authenticate(user.getUsername(), command.getConfirmingPassword());
        Account account = accountRepository.getById(accountId);
        account.changeEmail(command.getEmail(), emailPolicy);
        accountRepository.save(account);
        eventPublisher.publishEvent(new AccountEmailChangedEvent(account.getId(), account.getEmail()));
    }

    public void changePassword(User user, UUID accountId, ChangePasswordCommand command) {
        user.authorize(Permission.CHANGE_PASSWORD);
        authenticationService.authenticate(user.getUsername(), command.getConfirmingPassword());
        Account account = accountRepository.getById(accountId);
        account.changePassword(command.getNewPassword(), passwordPolicy);
        accountRepository.save(account);
    }

    public void assigneeRole(User user, UUID accountId, Role role) {
        user.authorize(Permission.ROLE_MANAGEMENT);
        Account account = accountRepository.getById(accountId);
        account.assignRole(role);
        accountRepository.save(account);
    }

    public void removeRole(User user, UUID accountId, Role role) {
        user.authorize(Permission.ROLE_MANAGEMENT);
        Account account = accountRepository.getById(accountId);
        account.removeRole(role);
        accountRepository.save(account);
    }

    public void deleteAccount(User user, UUID accountId) {
        user.authorize(Permission.DELETE_ACCOUNT);
        accountRepository.deleteById(accountId);
    }

}
