package com.fashionhouse.application.account;

import com.fashionhouse.application.account.command.AuthenticateCommand;
import com.fashionhouse.application.account.command.RegistrationCommand;
import com.fashionhouse.domain.account.event.AccountCreatedEvent;
import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.domain.account.model.AccountFactory;
import com.fashionhouse.domain.account.repository.AccountRepository;
import com.fashionhouse.domain.account.service.AuthenticationService;
import com.fashionhouse.infrastructure.annotations.ApplicationService;
import com.fashionhouse.infrastructure.event.producer.EventProducer;
import lombok.AllArgsConstructor;

@ApplicationService
@AllArgsConstructor
public class AccountFacade {

    private final AccountFactory accountFactory;
    private final AccountRepository accountRepository;
    private final AuthenticationService authenticationService;
    private final EventProducer eventPublisher;

    public String authenticate(AuthenticateCommand command) {
        return authenticationService.authenticate(command.getEmail(), command.getPassword());
    }

    public void register(RegistrationCommand command) {
        Account account = accountFactory.createForRegistration(command);
        accountRepository.save(account);
        eventPublisher.publishEvent(new AccountCreatedEvent(account.getId(), account.getEmail()));
    }

}
