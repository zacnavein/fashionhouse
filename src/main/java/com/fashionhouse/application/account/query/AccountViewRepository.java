package com.fashionhouse.application.account.query;

import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.domain.account.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class AccountViewRepository {

    private AccountRepository repository;

    public AccountView findById(UUID id) { // should return dto directly from database
        return map(repository.getById(id));
    }

    public List<AccountView> findAll() { // should return dto directly from database
        return repository.findAll().stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    private AccountView map(Account account) {
        return AccountView.builder()
                .id(account.getId())
                .email(account.getEmail())
                .roles(account.getRoles())
                .build();
    }

}
