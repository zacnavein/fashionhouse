package com.fashionhouse.domain.account.policy;

import com.fashionhouse.domain.account.ex.EmailAlreadyUsedException;
import com.fashionhouse.domain.account.ex.AccountAlreadyUsingEmailException;
import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.domain.account.repository.AccountRepository;
import com.fashionhouse.infrastructure.annotations.Policy;
import lombok.AllArgsConstructor;
import java.util.Optional;
import java.util.UUID;

@Policy
@AllArgsConstructor
public class EmailPolicyImpl implements EmailPolicy {

    private final AccountRepository accountRepository;

    @Override
    public void validateEmailUsed(String email) {
        accountRepository.findByEmail(email)
                .ifPresent(account -> { throw new AccountAlreadyUsingEmailException(String.format("account with email '%s' already exists", email)); });
    }

    @Override
    public void validateEmailUsed(UUID accountId, String email) {
        Optional<Account> foundAccount = accountRepository.findByEmail(email);
        if (foundAccount.isPresent()) {
            Account account = foundAccount.get();
            if (account.getEmail().equals(email) && account.getId().equals(accountId)) {
                throw new EmailAlreadyUsedException(String.format("account with id '%s' is already using email '%s'", accountId, email));
            }
            throw new AccountAlreadyUsingEmailException(String.format("account with email '%s' already exists", email));
        }
    }

}
