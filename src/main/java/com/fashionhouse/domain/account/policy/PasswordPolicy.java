package com.fashionhouse.domain.account.policy;

public interface PasswordPolicy {

    void validatePasswordStrength(String rawPassword);

    String encodePassword(String rawPassword);

}
