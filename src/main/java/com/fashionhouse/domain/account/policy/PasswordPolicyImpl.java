package com.fashionhouse.domain.account.policy;

import com.fashionhouse.infrastructure.annotations.Policy;
import com.fashionhouse.infrastructure.validator.impl.PasswordValidation;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

@Policy
@AllArgsConstructor
public class PasswordPolicyImpl implements PasswordPolicy {

    private static final PasswordValidation validation = new PasswordValidation();
    private final PasswordEncoder passwordEncoder;

    @Override
    public void validatePasswordStrength(String rawPassword) {
        validation.validatePassword(rawPassword);
    }

    @Override
    public String encodePassword(String rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

}
