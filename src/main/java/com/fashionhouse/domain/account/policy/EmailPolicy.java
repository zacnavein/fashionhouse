package com.fashionhouse.domain.account.policy;

import java.util.UUID;

public interface EmailPolicy {

    void validateEmailUsed(String email);

    void validateEmailUsed(UUID userId, String email);

}
