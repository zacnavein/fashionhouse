package com.fashionhouse.domain.account.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class WeakPasswordException extends ClientSideException {

    public WeakPasswordException(String message) {
        super(message);
    }

}
