package com.fashionhouse.domain.account.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class AccountAlreadyUsingEmailException extends ClientSideException {

    public AccountAlreadyUsingEmailException(String message) {
        super(message);
    }

}
