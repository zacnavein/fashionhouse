package com.fashionhouse.domain.account.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class AccountNotFoundException extends NotFoundException {

    public AccountNotFoundException(String message) {
        super(message);
    }

}
