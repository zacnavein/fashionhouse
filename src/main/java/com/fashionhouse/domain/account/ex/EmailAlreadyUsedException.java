package com.fashionhouse.domain.account.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class EmailAlreadyUsedException extends ClientSideException {

    public EmailAlreadyUsedException(String message) {
        super(message);
    }

}
