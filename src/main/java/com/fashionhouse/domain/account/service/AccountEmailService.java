package com.fashionhouse.domain.account.service;

import com.fashionhouse.domain.account.model.Account;

public interface AccountEmailService {

    void sendAccountCreatedEmail(Account account);

    void sendPasswordChangedEmail(Account account);

}
