package com.fashionhouse.domain.account.service;

public interface AuthenticationService {

    String authenticate(String login, String password);

}
