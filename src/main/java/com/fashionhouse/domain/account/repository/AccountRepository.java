package com.fashionhouse.domain.account.repository;

import com.fashionhouse.infrastructure.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface AccountRepository extends CrudAccountRepository, CustomAccountRepository {
}
