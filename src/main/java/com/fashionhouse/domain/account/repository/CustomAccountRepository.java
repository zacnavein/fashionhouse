package com.fashionhouse.domain.account.repository;

import com.fashionhouse.domain.account.ex.AccountNotFoundException;
import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.infrastructure.logger.Log;
import java.util.UUID;

@Log
public interface CustomAccountRepository {

    Account getById(UUID userId) throws AccountNotFoundException;

}
