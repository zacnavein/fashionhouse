package com.fashionhouse.domain.account.repository;

import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.infrastructure.logger.Log;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log
@Repository
public interface CrudAccountRepository extends CrudRepository<Account, UUID> {

    Optional<Account> findByEmail(String email);

    @Override
    List<Account> findAll();

}
