package com.fashionhouse.domain.account.event;

import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
public class AccountCreatedEvent {

    private UUID accountId;
    private String email;

}
