package com.fashionhouse.domain.account.model;

import com.fashionhouse.domain.account.policy.EmailPolicy;
import com.fashionhouse.domain.account.policy.PasswordPolicy;
import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.infrastructure.authentication.model.Role;
import com.google.common.collect.Sets;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@AggregateRoot
@Entity
@Builder
@Getter
@AllArgsConstructor
@Table(name = "accounts")
@ToString(exclude = "password")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Account {

    @Id
    @NotNull
    private UUID id;
    @Email
    @NotNull
    private String email;
    private String password;
    @Getter(AccessLevel.NONE)
    @ElementCollection(targetClass = Role.class)
    @CollectionTable(name = "roles")
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Set<Role> roles = Sets.newHashSet();

    public Set<Role> getRoles() {
        return Sets.newHashSet(roles);
    }

    public void assignRole(Role role) {
        roles.add(role);
    }

    public void removeRole(Role role) {
        roles.remove(role);
    }

    public void changeEmail(String email, EmailPolicy emailPolicy) {
        emailPolicy.validateEmailUsed(id, email);
        this.email = email;
    }

    public void changePassword(String newPassword, PasswordPolicy passwordPolicy) {
        passwordPolicy.validatePasswordStrength(newPassword);
        this.password = passwordPolicy.encodePassword(newPassword);
    }

}
