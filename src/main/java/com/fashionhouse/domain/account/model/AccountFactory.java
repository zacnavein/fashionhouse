package com.fashionhouse.domain.account.model;

import com.fashionhouse.application.account.command.RegistrationCommand;
import com.fashionhouse.domain.account.policy.EmailPolicy;
import com.fashionhouse.domain.account.policy.PasswordPolicy;
import com.fashionhouse.infrastructure.annotations.Factory;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Factory
@AllArgsConstructor
public class AccountFactory {

    private final EmailPolicy emailPolicy;
    private final PasswordPolicy passwordPolicy;

    public Account createForRegistration(RegistrationCommand command) {
        emailPolicy.validateEmailUsed(command.getEmail());
        passwordPolicy.validatePasswordStrength(command.getPassword());
        return Account.builder()
                .id(UUID.randomUUID())
                .email(command.getEmail())
                .password(passwordPolicy.encodePassword(command.getPassword()))
                .roles(Sets.newHashSet())
                .build();
    }

}
