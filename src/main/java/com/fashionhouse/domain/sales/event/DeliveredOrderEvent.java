package com.fashionhouse.domain.sales.event;

import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
public class DeliveredOrderEvent {

    private UUID orderId;
    private UUID requesterId;

}
