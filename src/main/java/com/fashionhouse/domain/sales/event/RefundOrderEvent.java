package com.fashionhouse.domain.sales.event;

import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@Value
@ToString
public class RefundOrderEvent {

    private UUID orderId;
    private UUID requesterId;

}
