package com.fashionhouse.domain.sales.event;

import com.fashionhouse.domain.sales.model.Delivery;
import com.fashionhouse.domain.sales.model.Invoice;
import com.fashionhouse.domain.sales.model.Recipient;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ToString
@Value
@Builder
public class SubmitOrderEvent {

    private UUID orderId;
    private UUID buyerId;

    private Recipient recipient;
    private Delivery delivery;
    private Invoice invoice;

    private List<OrderProductDetails> orderProducts;

    public OrderProductDetails find(UUID itemId) {
        return orderProducts.stream()
                .filter(orderProduct -> orderProduct.isProductId(itemId))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("item supply details not found"));
    }

    public List<UUID> getProductIds() {
        return orderProducts.stream()
                .map(OrderProductDetails::getProductId)
                .collect(Collectors.toList());
    }

}
