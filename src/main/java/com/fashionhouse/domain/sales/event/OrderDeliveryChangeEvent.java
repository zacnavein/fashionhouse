package com.fashionhouse.domain.sales.event;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
@Builder
public class OrderDeliveryChangeEvent {

    private UUID orderId;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
