package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class AddressNotFoundException extends NotFoundException {

    public AddressNotFoundException(String message) {
        super(message);
    }

}
