package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class ProductWithoutPriceException extends ClientSideException {

    public ProductWithoutPriceException(String message) {
        super(message);
    }

}
