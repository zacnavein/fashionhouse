package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.UnprocessableException;

public class ProductOutOfStockException extends UnprocessableException {

    public ProductOutOfStockException(String message) {
        super(message);
    }

}
