package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.UnprocessableException;

public class InvalidOrderStateException extends UnprocessableException {

    public InvalidOrderStateException(String message) {
        super(message);
    }

}
