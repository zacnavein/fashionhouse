package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class OrderNotFoundException extends NotFoundException {

    public OrderNotFoundException(String message) {
        super(message);
    }

}
