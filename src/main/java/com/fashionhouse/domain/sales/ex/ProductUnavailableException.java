package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class ProductUnavailableException extends ClientSideException {

    public ProductUnavailableException(String message) {
        super(message);
    }

}
