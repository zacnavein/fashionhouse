package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.UnprocessableException;

public class UnmodifiableOrderException extends UnprocessableException {

    public UnmodifiableOrderException(String message) {
        super(message);
    }

}
