package com.fashionhouse.domain.sales.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class ProductNotFoundException extends NotFoundException {

    public ProductNotFoundException(String message) {
        super(message);
    }

}
