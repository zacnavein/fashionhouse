package com.fashionhouse.domain.sales.model;

public enum ProductStatus {

    AVAILABLE,
    UNAVAILABLE

}
