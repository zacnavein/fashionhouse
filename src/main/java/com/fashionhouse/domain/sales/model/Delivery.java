package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@ValueObject
@Getter
@Builder
@AllArgsConstructor
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Delivery {

    @NotNull
    private String postcode;
    @NotNull
    private String city;
    @NotNull
    private String street;
    private String description;

}
