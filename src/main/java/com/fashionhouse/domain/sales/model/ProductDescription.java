package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductDescription {

    @NotNull
    private String name;
    @NotNull
    private Double price;
    private String description;

}
