package com.fashionhouse.domain.sales.model;

import com.fashionhouse.application.sales.command.CreateProductCommand;
import com.fashionhouse.application.sales.command.ChangeProductCommand;
import org.springframework.stereotype.Component;

@Component
public class ProductFactory {

    public Product createProduct(CreateProductCommand command) {
        return Product.builder()
                .id(command.getProductId())
                .stock(0)
                .status(ProductStatus.UNAVAILABLE)
                .product(productDescription(command))
                .build();
    }

    public ProductDetails createProductDetails(Product product) {
        return ProductDetails.builder()
                .productId(product.getId())
                .name(product.getDescription().getName())
                .price(product.getDescription().getPrice())
                .description(product.getDescription().getDescription())
                .build();
    }

    public ProductDescription createProductDescription(ChangeProductCommand command) {
        return ProductDescription.builder()
                .name(command.getName())
                .price(command.getPrice())
                .description(command.getDescription())
                .build();
    }

    private ProductDescription productDescription(CreateProductCommand command) {
        return ProductDescription.builder()
                .name(command.getName())
                .description(command.getDescription())
                .build();
    }

}
