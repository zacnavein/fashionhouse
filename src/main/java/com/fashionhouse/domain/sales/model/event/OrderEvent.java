package com.fashionhouse.domain.sales.model.event;

import com.fashionhouse.infrastructure.util.TimeProvider;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.UUID;

@ToString
@Getter
@EqualsAndHashCode(of = "id")
@Entity(name = "order_events")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class OrderEvent {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private LocalDateTime date;

    OrderEvent() {
        this.id = UUID.randomUUID();
        this.date = TimeProvider.currentDateTime();
    }

    public static int sortNewest(OrderEvent event1, OrderEvent event2) {
        return event2.getDate().compareTo(event1.getDate());
    }

}
