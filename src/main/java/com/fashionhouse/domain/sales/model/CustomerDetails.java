package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import jakarta.persistence.Embeddable;
import lombok.*;

@ValueObject
@Builder
@ToString
@Getter
@Embeddable
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerDetails {

    private String email;
    private String phone;
    private String firstName;
    private String lastName;

}
