package com.fashionhouse.domain.sales.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.UUID;

@Entity
@ToString
@Getter
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Table(name="customer_addresses")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Address {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private String postcode;
    @NotNull
    private String city;
    @NotNull
    private String street;
    private String description;

}
