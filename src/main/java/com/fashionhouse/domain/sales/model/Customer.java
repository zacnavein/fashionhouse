package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.domain.sales.ex.AddressNotFoundException;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import java.util.Set;
import java.util.UUID;

@AggregateRoot
@Entity
@Builder
@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Table(name="customers")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Customer {

    @Id
    @NotNull
    private UUID id;
    @Setter
    @Embedded
    private CustomerDetails customer;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "customer_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Address> addresses;

    public void changeEmail(String email) {
        customer = CustomerDetails.builder()
                .email(email)
                .build();
    }

    public void changeDetails(CustomerDetails customerDetails) {
        customer = customerDetails;
    }

    public void addAddress(Address address) {
        addresses.add(address);
    }

    public void removeAddress(UUID addressId) {
        addresses.remove(findAddress(addressId));
    }

    private Address findAddress(UUID addressId) {
        return addresses.stream()
                .filter(address -> address.getId().equals(addressId))
                .findAny()
                .orElseThrow(() -> new AddressNotFoundException(String.format("address '%s' not found for customer '%s'", addressId, id)));
    }

}
