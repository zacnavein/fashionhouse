package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.util.UUID;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductDetails {

    @NotNull
    @Column(name = "product_id")
    private UUID productId;
    @NotNull
    private String name;
    @NotNull
    private Double price;
    private String description;

}
