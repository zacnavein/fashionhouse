package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.domain.sales.ex.InvalidOrderStateException;
import com.fashionhouse.domain.sales.ex.UnmodifiableOrderException;
import com.fashionhouse.domain.sales.model.event.OrderEvent;
import com.fashionhouse.domain.sales.model.event.OrderStatusEvent;
import com.google.common.collect.Sets;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@Table(name="orders")
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Order {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID buyerId;
    @NotNull
    @Embedded
    private Recipient recipient;
    @Embedded
    private Invoice invoice;
    @NotNull
    @Embedded
    private Delivery delivery;
    @NotEmpty
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "order_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderProduct> orderProducts;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "order_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderEvent> events;

    public List<OrderStatusEvent> getStatusEvents() {
        return events.stream()
                .filter(this::isStatusEvent)
                .sorted(OrderEvent::sortNewest)
                .map(event -> ((OrderStatusEvent) event))
                .collect(Collectors.toList());
    }

    private boolean isStatusEvent(OrderEvent event) {
        return event instanceof OrderStatusEvent;
    }

    public Set<OrderProduct> getOrderProducts() {
        return Sets.newHashSet(orderProducts);
    }

    public void changeInvoice(Invoice invoice) {
        validateOrderModifiable();
        this.invoice = invoice;
    }

    public void changeDelivery(Delivery delivery) {
        validateOrderModifiable();
        this.delivery = delivery;
    }

    public void packed() {
        validateOrderNotFinished();
        events.add(OrderStatusEvent.createPackedEvent());
    }

    public void returned() {
        validateOrderNotFinished();
        events.add(OrderStatusEvent.createReturnedEvent());
    }

    public void sent() {
        validateOrderNotFinished();
        events.add(OrderStatusEvent.createSentEvent());
    }

    public void delivered() {
        validateOrderNotFinished();
        events.add(OrderStatusEvent.createDeliveredEvent());
    }

    public void refund() {
        validateOrderNotRefunded();
        events.add(OrderStatusEvent.createRefundEvent());
    }

    private void validateOrderModifiable() {
        if (!isOrderModifiable()) {
            throw new UnmodifiableOrderException(String.format("order '%s' is unmodifiable", id));
        }
    }

    private void validateOrderNotFinished() {
        if (isFinished()) {
            throw new InvalidOrderStateException(String.format("order '%s' is finished", id));
        }
    }

    private void validateOrderNotRefunded() {
        if (isRefunded()) {
            throw new InvalidOrderStateException(String.format("order '%s' is refunded", id));
        }
    }

    private boolean isOrderModifiable() {
        OrderStatus currentStatus = getCurrentStatus();
        return currentStatus == OrderStatus.SUBMITTED || currentStatus == OrderStatus.PACKED;
    }

    private boolean isFinished() {
        OrderStatus currentStatus = getCurrentStatus();
        return currentStatus == OrderStatus.REFUND || currentStatus == OrderStatus.DELIVERED;
    }

    private boolean isRefunded() {
        return getStatusEvents().stream().anyMatch(statusEvent -> statusEvent.hasStatus(OrderStatus.REFUND));
    }

    private OrderStatus getCurrentStatus() {
        return getStatusEvents().stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("order '%s' without status", id)))
                .getStatus();
    }

}
