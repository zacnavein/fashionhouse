package com.fashionhouse.domain.sales.model;

import com.fashionhouse.application.sales.command.CustomerAddressCommand;
import org.springframework.stereotype.Component;

@Component
public class AddressFactory {

    public Address create(CustomerAddressCommand command) {
        return Address.builder()
                .postcode(command.getPostcode())
                .city(command.getCity())
                .street(command.getStreet())
                .description(command.getDescription())
                .build();
    }

}
