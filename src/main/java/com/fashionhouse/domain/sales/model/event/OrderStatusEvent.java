package com.fashionhouse.domain.sales.model.event;

import com.fashionhouse.domain.sales.model.OrderStatus;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("OrderStatus")
public class OrderStatusEvent extends OrderEvent {

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    public OrderStatusEvent(OrderStatus status) {
        this();
        this.status = status;
    }

    public static OrderStatusEvent createRefundEvent() {
        return new OrderStatusEvent(OrderStatus.REFUND);
    }

    public static OrderStatusEvent createSubmittedEvent() {
        return new OrderStatusEvent(OrderStatus.SUBMITTED);
    }

    public static OrderStatusEvent createPackedEvent() {
        return new OrderStatusEvent(OrderStatus.PACKED);
    }

    public static OrderStatusEvent createSentEvent() {
        return new OrderStatusEvent(OrderStatus.SENT);
    }

    public static OrderStatusEvent createReturnedEvent() {
        return new OrderStatusEvent(OrderStatus.RETURNED);
    }

    public static OrderStatusEvent createDeliveredEvent() {
        return new OrderStatusEvent(OrderStatus.DELIVERED);
    }

    public boolean hasStatus(OrderStatus status) {
        return this.status == status;
    }

}
