package com.fashionhouse.domain.sales.model;

import com.fashionhouse.application.sales.command.*;
import com.fashionhouse.infrastructure.annotations.Factory;
import com.fashionhouse.domain.sales.model.event.OrderStatusEvent;
import com.fashionhouse.infrastructure.util.TimeProvider;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import java.util.Set;
import java.util.UUID;

@Factory
@AllArgsConstructor
public class OrderFactory {

    private final OrderProductFactory orderProductFactory;

    public Order createOrderToSubmit(UUID buyerId, SubmitOrderCommand command) {
        Set<OrderProduct> orderProducts = orderProductFactory.createOrderProducts(command.getProducts());
        return Order.builder()
                .id(UUID.randomUUID())
                .buyerId(buyerId)
                .orderProducts(orderProducts)
                .recipient(createRecipient(command.getRecipient()))
                .delivery(createDelivery(command.getDelivery()))
                .invoice(createInvoice(command.getInvoice()))
                .events(Sets.newHashSet(new OrderStatusEvent(OrderStatus.SUBMITTED)))
                .build();
    }

    public Recipient createRecipient(RecipientCommand command) {
        return Recipient.builder()
                .firstName(command.getFirstName())
                .lastName(command.getLastName())
                .email(command.getEmail())
                .phone(command.getPhone())
                .build();
    }

    public Invoice createInvoice(ChangeInvoiceCommand command) {
        return Invoice.builder()
                .name(command.getName())
                .nip(command.getNip())
                .address(command.getAddress())
                .date(TimeProvider.currentDateTime())
                .build();
    }

    public Delivery createDelivery(ChangeOrderDeliveryCommand command) {
        return Delivery.builder()
                .postcode(command.getPostcode())
                .city(command.getCity())
                .street(command.getStreet())
                .description(command.getDescription())
                .build();
    }

    private Invoice createInvoice(InvoiceCommand command) {
        return Invoice.builder()
                .name(command.getName())
                .nip(command.getNip())
                .address(command.getAddress())
                .date(TimeProvider.currentDateTime())
                .build();
    }

    private Delivery createDelivery(OrderDeliveryCommand command) {
        return Delivery.builder()
                .postcode(command.getPostcode())
                .city(command.getCity())
                .street(command.getStreet())
                .description(command.getDescription())
                .build();
    }

}
