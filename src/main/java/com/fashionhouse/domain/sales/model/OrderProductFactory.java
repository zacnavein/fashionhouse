package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.Factory;
import com.fashionhouse.application.sales.command.OrderProductCommand;
import com.fashionhouse.domain.sales.repository.ProductRepository;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Factory
@AllArgsConstructor
public class OrderProductFactory {

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;

    public Set<OrderProduct> createOrderProducts(List<OrderProductCommand> commands) {
        return commands.stream().map(this::createOrderProduct).collect(Collectors.toSet());
    }

    public OrderProduct createOrderProduct(OrderProductCommand command) {
        return createOrderProduct(command.getProductId(), command.getAmount());
    }

    public OrderProduct createOrderProduct(UUID productId, Integer amount) {
        Product product = productRepository.getById(productId);
        product.validateSaleability(amount);
        return OrderProduct.builder()
                .id(UUID.randomUUID())
                .product(productFactory.createProductDetails(product))
                .amount(amount)
                .build();
    }

}
