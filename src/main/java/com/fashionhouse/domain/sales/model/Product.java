package com.fashionhouse.domain.sales.model;

import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.domain.sales.ex.ProductOutOfStockException;
import com.fashionhouse.domain.sales.ex.ProductUnavailableException;
import com.fashionhouse.domain.sales.ex.ProductWithoutPriceException;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import java.util.UUID;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@AllArgsConstructor
@Table(name="products")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    @Embedded
    private ProductDescription product;
    @NotNull
    private Integer stock;
    @NotNull
    @Enumerated(EnumType.STRING)
    private ProductStatus status;

    public ProductDescription getDescription() {
        return product;
    }

    public void changeDescription(ProductDescription description) {
        this.product = description;
    }

    public void updateStock(Integer stock) {
        this.stock = stock;
    }

    public void validateSaleability(Integer amount) {
        validateAvailability();
        validatePrice();
        validateStock(amount);
    }

    public void available() {
        validatePrice();
        status = ProductStatus.AVAILABLE;
    }

    public void unavailable() {
        status = ProductStatus.UNAVAILABLE;
    }

    private void validateStock(Integer quantity) {
        if (stock < quantity) {
            throw new ProductOutOfStockException(String.format("not enough product '%s' in stock", id));
        }
    }

    private void validateAvailability() {
        if (status == ProductStatus.UNAVAILABLE) {
            throw new ProductUnavailableException(String.format("product '%s' is unavailable", id));
        }
    }

    private void validatePrice() {
        if (product.getPrice() == null) {
            throw new ProductWithoutPriceException(String.format("product '%s' without price", id));
        }
    }

}
