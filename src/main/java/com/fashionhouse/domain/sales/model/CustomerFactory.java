package com.fashionhouse.domain.sales.model;

import com.fashionhouse.application.sales.command.CreateCustomerCommand;
import com.fashionhouse.application.sales.command.UpdateCustomerCommand;
import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;

@Component
public class CustomerFactory {

    public Customer createCustomer(CreateCustomerCommand event) {
        return Customer.builder()
                .id(event.getAccountId())
                .customer(
                        CustomerDetails.builder()
                                .email(event.getEmail())
                                .build())
                .addresses(Sets.newHashSet())
                .build();
    }

    public CustomerDetails createCustomerDetails(UpdateCustomerCommand command) {
        return CustomerDetails.builder()
                .email(command.getEmail())
                .phone(command.getPhone())
                .firstName(command.getFirstName())
                .lastName(command.getLastName())
                .build();
    }

}
