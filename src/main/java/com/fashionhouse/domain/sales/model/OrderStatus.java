package com.fashionhouse.domain.sales.model;

public enum OrderStatus {

    REFUND,
    SUBMITTED,
    PACKED,
    SENT,
    RETURNED,
    DELIVERED

}