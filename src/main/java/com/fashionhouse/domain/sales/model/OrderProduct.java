package com.fashionhouse.domain.sales.model;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.UUID;

@Entity
@Getter
@Builder
@ToString
@EqualsAndHashCode(of = "id")
@Table(name="order_products")
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderProduct {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    @Min(1)
    private Integer amount;
    @NotNull
    @Embedded
    private ProductDetails product;

}
