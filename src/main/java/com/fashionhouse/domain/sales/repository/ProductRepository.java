package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface ProductRepository extends CrudProductRepository, CustomProductRepository {
}
