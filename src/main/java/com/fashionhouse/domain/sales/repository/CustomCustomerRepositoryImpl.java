package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.domain.sales.ex.OrderNotFoundException;
import com.fashionhouse.domain.sales.model.Customer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class CustomCustomerRepositoryImpl implements CustomCustomerRepository {

    private final CrudCustomerRepository customerRepository;

    @Override
    public Customer getById(UUID customerId) {
        return customerRepository.findById(customerId).orElseThrow(() -> new OrderNotFoundException(String.format("customer with id '%s' not found", customerId)));
    }

}
