package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.sales.model.Order;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log
public interface CrudOrderRepository extends CrudRepository<Order, UUID> {

    List<Order> findByBuyerId(UUID buyerId);

    Optional<Order> findByIdAndBuyerId(UUID id, UUID buyerId);

    List<Order> findAll();

}
