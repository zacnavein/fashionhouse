package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface CustomerRepository extends CrudCustomerRepository, CustomCustomerRepository {
}
