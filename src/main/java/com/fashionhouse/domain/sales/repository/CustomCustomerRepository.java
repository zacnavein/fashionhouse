package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.sales.model.Customer;
import java.util.UUID;

@Log
public interface CustomCustomerRepository {

    Customer getById(UUID customerId);

}
