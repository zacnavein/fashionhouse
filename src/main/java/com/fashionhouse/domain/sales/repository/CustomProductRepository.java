package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.sales.model.Product;

import java.util.Map;
import java.util.UUID;

@Log
public interface CustomProductRepository {

    Product getById(UUID productId);

    Map<UUID, Product> getProductsById(Iterable<UUID> ids);

    Iterable<Product> saveAll(Map<UUID, Product> products);

}
