package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.sales.model.Order;
import java.util.UUID;

@Log
public interface CustomOrderRepository {

    Order getById(UUID orderId);

    Order getByIdAndBuyerId(UUID orderId, UUID buyerId);

}
