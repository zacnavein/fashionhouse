package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.sales.model.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

@Log
public interface CrudCustomerRepository extends CrudRepository<Customer, UUID> {

    List<Customer> findAll();

}
