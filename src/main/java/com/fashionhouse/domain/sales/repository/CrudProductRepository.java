package com.fashionhouse.domain.sales.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.sales.model.Product;
import com.fashionhouse.domain.sales.model.ProductStatus;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.UUID;

@Log
public interface CrudProductRepository extends CrudRepository<Product, UUID> {

    List<Product> findByStatus(ProductStatus status);

    @Override
    List<Product> findAllById(Iterable<UUID> ids);

    List<Product> findAll();

}
