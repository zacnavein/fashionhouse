package com.fashionhouse.domain.design.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
@Builder
@AllArgsConstructor
public class DesignApprovedEvent {

    private UUID designId;
    private UUID designerId;
    private UUID verifierId;
    private String name;
    private String type;
    private String description;
    private String url;

}
