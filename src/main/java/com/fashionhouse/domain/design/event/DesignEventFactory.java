package com.fashionhouse.domain.design.event;

import com.fashionhouse.domain.design.model.Design;
import org.springframework.stereotype.Component;

@Component
public class DesignEventFactory {

    public DesignApprovedEvent createDesignApprovedEvent(Design design) {
        return DesignApprovedEvent.builder()
                .designId(design.getId())
                .name(design.getDesign().getName())
                .type(design.getDesign().getType())
                .url(design.getDesign().getUrl())
                .description(design.getDesign().getDescription())
                .designerId(design.getDesignerId())
                .verifierId(design.getVerifierId())
                .build();
    }

}
