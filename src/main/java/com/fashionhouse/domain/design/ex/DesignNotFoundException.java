package com.fashionhouse.domain.design.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class DesignNotFoundException extends NotFoundException {

    public DesignNotFoundException(String message) {
        super(message);
    }

}
