package com.fashionhouse.domain.design.ex;

import com.fashionhouse.infrastructure.ex.UnprocessableException;

public class InvalidDesignStatusException extends UnprocessableException {

    public InvalidDesignStatusException(String message) {
        super(message);
    }

}
