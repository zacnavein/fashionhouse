package com.fashionhouse.domain.design.model;

public enum DesignStatus {

    CANCELED,
    PROTOTYPED,
    FINISHED,
    APPROVED,
    DECLINED

}
