package com.fashionhouse.domain.design.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import com.fashionhouse.infrastructure.validator.annotations.URL;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@ValueObject
@Builder
@Getter
@AllArgsConstructor
@ToString
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DesignDetails {

    @NotNull
    private String name;
    @NotNull
    private String type;
    @NotNull
    private String description;
    @URL
    @NotNull
    private String url;

}
