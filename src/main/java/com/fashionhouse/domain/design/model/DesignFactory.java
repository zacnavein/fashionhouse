package com.fashionhouse.domain.design.model;

import com.fashionhouse.infrastructure.annotations.Factory;
import com.fashionhouse.infrastructure.util.TimeProvider;
import com.fashionhouse.application.design.command.DesignCommand;
import lombok.AllArgsConstructor;
import java.util.UUID;

@Factory
@AllArgsConstructor
public class DesignFactory {

    public Design createDesign(UUID designerId, DesignCommand command) {
        return Design.builder()
                .id(UUID.randomUUID())
                .designerId(designerId)
                .createdAt(TimeProvider.currentDateTime())
                .design(createDesignDetails(command))
                .status(DesignStatus.PROTOTYPED)
                .build();
    }

    public DesignDetails createDesignDetails(DesignCommand command) {
        return DesignDetails.builder()
                .name(command.getName())
                .type(command.getType())
                .description(command.getDescription())
                .url(command.getUrl())
                .build();
    }

}
