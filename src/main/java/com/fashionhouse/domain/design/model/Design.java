package com.fashionhouse.domain.design.model;

import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.infrastructure.util.TimeProvider;
import com.fashionhouse.domain.design.ex.InvalidDesignStatusException;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@AggregateRoot
@Entity
@Builder
@Getter
@ToString
@AllArgsConstructor
@Table(name="designs")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Design {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private DesignDetails design;
    @NotNull
    private UUID designerId;
    private UUID verifierId;
    @NotNull
    @Enumerated(EnumType.STRING)
    private DesignStatus status;
    @NotNull
    private LocalDateTime createdAt;
    private LocalDateTime verifiedAt;

    public void changeDesign(DesignDetails design) {
        validateModifiable();
        this.design = design;
    }

    public void cancel() {
        validateModifiable();
        status = DesignStatus.CANCELED;
    }

    public void finish() {
        validateModifiable();
        status = DesignStatus.FINISHED;
        verifiedAt = TimeProvider.currentDateTime();
    }

    public void approve(UUID approverId) {
        validateVerifiable();
        verifierId = approverId;
        status = DesignStatus.APPROVED;
        verifiedAt = TimeProvider.currentDateTime();
    }

    public void decline(UUID declineId) {
        validateVerifiable();
        verifierId = declineId;
        status = DesignStatus.DECLINED;
        verifiedAt = TimeProvider.currentDateTime();
    }

    private void validateModifiable() {
        if (!isModifiable()) {
            throw new InvalidDesignStatusException("design can not be modified");
        }
    }

    private void validateVerifiable() {
        if (!isVerifiable()) {
            throw new InvalidDesignStatusException("design can not be verified");
        }
    }

    private boolean isModifiable() {
        return status == DesignStatus.PROTOTYPED;
    }

    private boolean isVerifiable() {
        return status == DesignStatus.FINISHED;
    }

}
