package com.fashionhouse.domain.design.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.design.model.Design;
import java.util.UUID;

@Log
public interface CustomDesignRepository {

    Design getById(UUID designId);

    Design getByIdAndDesignerId(UUID designId, UUID designerId);

}
