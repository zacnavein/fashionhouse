package com.fashionhouse.domain.design.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.design.model.Design;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log
public interface CrudDesignRepository extends CrudRepository<Design, UUID> {

    Optional<Design> findByIdAndDesignerId(UUID designId, UUID designerId);

    @Override
    List<Design> findAll();

}
