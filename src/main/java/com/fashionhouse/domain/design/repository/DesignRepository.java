package com.fashionhouse.domain.design.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface DesignRepository extends CrudDesignRepository, CustomDesignRepository {
}
