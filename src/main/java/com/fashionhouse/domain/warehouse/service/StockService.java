package com.fashionhouse.domain.warehouse.service;

import com.fashionhouse.infrastructure.annotations.DomainService;
import com.fashionhouse.domain.warehouse.model.Item;
import com.fashionhouse.domain.warehouse.model.Parcel;
import lombok.AllArgsConstructor;
import java.util.Map;
import java.util.UUID;

@DomainService
@AllArgsConstructor
public class StockService {

    public void drawItemsToParcel(Parcel parcel, Map<UUID, Item> items) {
        parcel.getParcelItems().forEach(parcelItem -> items.get(parcelItem.getItemId()).draw(parcelItem.getAmount()));
    }

    public void supplyItemsFromParcel(Parcel parcel, Map<UUID, Item> items) {
        parcel.getParcelItems().forEach(parcelItem -> items.get(parcelItem.getItemId()).supply(parcelItem.getAmount()));
    }

}
