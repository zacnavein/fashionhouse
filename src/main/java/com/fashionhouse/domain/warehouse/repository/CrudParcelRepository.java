package com.fashionhouse.domain.warehouse.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.warehouse.model.Parcel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log
public interface CrudParcelRepository extends CrudRepository<Parcel, UUID> {

    Optional<Parcel> findByOrderId(UUID orderId);

    List<Parcel> findAll();

}
