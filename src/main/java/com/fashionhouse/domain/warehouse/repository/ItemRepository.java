package com.fashionhouse.domain.warehouse.repository;

import com.fashionhouse.infrastructure.logger.Log;
import org.springframework.stereotype.Repository;

@Log
@Repository
public interface ItemRepository extends CrudItemRepository, CustomItemRepository {
}
