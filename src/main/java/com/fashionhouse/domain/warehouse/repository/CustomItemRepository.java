package com.fashionhouse.domain.warehouse.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.warehouse.model.Item;
import java.util.Map;
import java.util.UUID;

@Log
public interface CustomItemRepository {

    Item getById(UUID itemId);

    Map<UUID, Item> getItemsById(Iterable<UUID> ids);

    Iterable<Item> saveAll(Map<UUID, Item> items);

}
