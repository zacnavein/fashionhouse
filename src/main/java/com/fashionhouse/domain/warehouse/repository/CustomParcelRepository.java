package com.fashionhouse.domain.warehouse.repository;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.warehouse.model.Parcel;
import java.util.UUID;

@Log
public interface CustomParcelRepository {

    Parcel getById(UUID parcelId);

    Parcel getByOrderId(UUID orderId);

}
