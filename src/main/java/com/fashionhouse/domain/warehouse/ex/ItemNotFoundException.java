package com.fashionhouse.domain.warehouse.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class ItemNotFoundException extends NotFoundException {

    public ItemNotFoundException(String message) {
        super(message);
    }

}
