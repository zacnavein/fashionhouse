package com.fashionhouse.domain.warehouse.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class ParcelNotFoundException extends NotFoundException {

    public ParcelNotFoundException(String message) {
        super(message);
    }

}
