package com.fashionhouse.domain.warehouse.ex;

import com.fashionhouse.infrastructure.ex.UnprocessableException;

public class InvalidParcelStateException extends UnprocessableException {

    public InvalidParcelStateException(String message) {
        super(message);
    }

}
