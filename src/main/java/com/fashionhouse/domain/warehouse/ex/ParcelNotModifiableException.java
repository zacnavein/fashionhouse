package com.fashionhouse.domain.warehouse.ex;

import com.fashionhouse.infrastructure.ex.NotFoundException;

public class ParcelNotModifiableException extends NotFoundException {

    public ParcelNotModifiableException(String message) {
        super(message);
    }

}
