package com.fashionhouse.domain.warehouse.ex;

import com.fashionhouse.infrastructure.ex.UnprocessableException;

public class ItemOutOfStockException extends UnprocessableException {

    public ItemOutOfStockException(String message) {
        super(message);
    }

}
