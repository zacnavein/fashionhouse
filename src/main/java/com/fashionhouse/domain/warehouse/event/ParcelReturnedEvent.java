package com.fashionhouse.domain.warehouse.event;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
@Builder
public class ParcelReturnedEvent {

    private UUID parcelId;
    private UUID orderId;

}
