package com.fashionhouse.domain.warehouse.event;

import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.Value;
import java.util.UUID;

@ToString
@Value
@AllArgsConstructor
public class UnrealizableParcelEvent {

    private UUID orderId;

}
