package com.fashionhouse.domain.warehouse.event;

import lombok.ToString;
import lombok.Value;

import java.util.List;

@ToString
@Value
public class ItemSupplyEvent {

    private List<ItemSupplyDetails> supplies;

}
