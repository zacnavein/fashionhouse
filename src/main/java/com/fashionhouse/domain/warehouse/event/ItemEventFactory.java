package com.fashionhouse.domain.warehouse.event;

import com.fashionhouse.infrastructure.logger.Log;
import com.fashionhouse.domain.warehouse.model.Item;
import com.fashionhouse.domain.warehouse.model.Parcel;
import com.fashionhouse.domain.warehouse.model.ParcelItem;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Log
@Component
public class ItemEventFactory {

    public ItemSupplyEvent createSupplyEvent(Item item, Integer amount) {
        return new ItemSupplyEvent(Lists.newArrayList(new ItemSupplyDetails(item.getId(), item.getStock(), amount)));
    }

    public ItemDrawEvent createDrawEvent(Item item, Integer amount) {
        return new ItemDrawEvent(Lists.newArrayList(new ItemDrawDetails(item.getId(), item.getStock(), amount)));
    }

    public ItemSupplyEvent createSupplyEvent(Parcel parcel, Map<UUID, Item> items) {
        return new ItemSupplyEvent(
                parcel.getParcelItems().stream()
                        .map(parcelItem -> createItemSupplyDetails(parcelItem, items))
                        .collect(Collectors.toList()
                        ));
    }

    public ItemDrawEvent createDrawEvent(Parcel parcel, Map<UUID, Item> items) {
        return new ItemDrawEvent(
                parcel.getParcelItems().stream()
                        .map(parcelItem -> createItemDrawDetails(parcelItem, items))
                        .collect(Collectors.toList()
                        ));
    }

    private ItemDrawDetails createItemDrawDetails(ParcelItem parcelItem, Map<UUID, Item> items) {
        return ItemDrawDetails.builder()
                .itemId(parcelItem.getItemId())
                .draw(parcelItem.getAmount())
                .stock(items.get(parcelItem.getItemId()).getStock())
                .build();
    }

    private ItemSupplyDetails createItemSupplyDetails(ParcelItem parcelItem, Map<UUID, Item> items) {
        return ItemSupplyDetails.builder()
                .itemId(parcelItem.getItemId())
                .supplied(parcelItem.getAmount())
                .stock(items.get(parcelItem.getItemId()).getStock())
                .build();
    }

}
