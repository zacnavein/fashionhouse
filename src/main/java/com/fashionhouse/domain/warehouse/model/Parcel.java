package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.domain.warehouse.ex.InvalidParcelStateException;
import com.fashionhouse.domain.warehouse.ex.ParcelNotModifiableException;
import com.fashionhouse.domain.warehouse.model.event.ParcelEvent;
import com.fashionhouse.domain.warehouse.model.event.ParcelStatusEvent;
import com.fashionhouse.infrastructure.validator.ValidationUtil;
import com.google.common.collect.Sets;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@AggregateRoot
@Entity
@Getter
@Builder
@AllArgsConstructor
@ToString
@Table(name = "parcels")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Parcel {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID orderId;
    @NotNull
    private UUID recipientId;
    @Embedded
    @NotNull
    private Recipient recipient;
    @Embedded
    private Invoice invoice;
    @Embedded
    @NotNull
    private Destination destination;
    @Embedded
    private Measurement measurement;
    @NotEmpty
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "parcel_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ParcelItem> parcelItems;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "parcel_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ParcelEvent> events;

    public List<ParcelStatusEvent> getStatusEvents() {
        return events.stream()
                .filter(this::isStatusEvent)
                .sorted(ParcelEvent::sortNewest)
                .map(event -> ((ParcelStatusEvent) event))
                .collect(Collectors.toList());
    }

    private boolean isStatusEvent(ParcelEvent event) {
        return event instanceof ParcelStatusEvent;
    }

    public boolean isOwner(UUID userId) {
        return recipientId == userId;
    }

    public Set<UUID> getParcelItemIds() {
        return parcelItems.stream()
                .map(ParcelItem::getItemId)
                .collect(Collectors.toSet());
    }

    public Set<ParcelItem> getParcelItems() {
        return Sets.newHashSet(parcelItems);
    }

    public void changeDestination(Destination destination) {
        validateParcelModifiable();
        this.destination = destination;
        ValidationUtil.validate(this);
    }

    public void changeInvoice(Invoice invoice) {
        validateParcelModifiable();
        this.invoice = invoice;
        ValidationUtil.validate(this);
    }

    public void cancel() {
        validateNotCanceled();
        events.add(new ParcelStatusEvent(ParcelStatus.CANCELLED));
    }

    public void packed(Measurement measurement) {
        validateParcelNotFinished();
        this.measurement = measurement;
        events.add(new ParcelStatusEvent(ParcelStatus.PACKED));
    }

    public void sent() {
        validateParcelNotFinished();
        events.add(new ParcelStatusEvent(ParcelStatus.SENT));
    }

    public void delivered() {
        validateParcelNotFinished();
        events.add(new ParcelStatusEvent(ParcelStatus.DELIVERED));
    }

    public void returned() {
        validateParcelNotFinished();
        events.add(new ParcelStatusEvent(ParcelStatus.RETURNED));
    }

    private void validateParcelNotFinished() {
        if (isFinished()) {
            throw new InvalidParcelStateException("parcel is finished");
        }
    }

    private void validateNotCanceled() {
        if (isCanceled()) {
            throw new InvalidParcelStateException(String.format("parcel '%s' is canceled", id) );
        }
    }

    private void validateParcelModifiable() {
        if (!isParcelModifiable()) {
            throw new ParcelNotModifiableException(String.format("parcel '%s' is unmodifiable", id));
        }
    }

    private boolean isParcelModifiable() {
        ParcelStatus currentStatus = getCurrentStatus();
        return currentStatus == ParcelStatus.AWAIT || currentStatus == ParcelStatus.PACKED;
    }

    private boolean isCanceled() {
        return getCurrentStatus() == ParcelStatus.CANCELLED;
    }

    private boolean isFinished() {
        ParcelStatus currentStatus = getCurrentStatus();
        return currentStatus == ParcelStatus.CANCELLED || currentStatus == ParcelStatus.DELIVERED;
    }

    private ParcelStatus getCurrentStatus() {
        return getStatusEvents().stream()
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("parcel '%s' without status", id)))
                .getStatus();
    }

}
