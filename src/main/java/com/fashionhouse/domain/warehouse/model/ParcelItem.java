package com.fashionhouse.domain.warehouse.model;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import java.util.UUID;

@Entity
@Getter
@Builder
@ToString
@AllArgsConstructor
@Table(name="parcel_items")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParcelItem {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private UUID itemId;
    @NotNull
    @Min(1)
    private Integer amount;
    @Embedded
    @NotNull
    private ItemDescription item;

}
