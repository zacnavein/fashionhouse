package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import com.fashionhouse.infrastructure.validator.ValidationUtil;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ItemDescription {

    @NotNull
    private String name;
    @NotNull
    private String comment;

    private ItemDescription(ItemDescription.ItemDescriptionBuilder builder) {
        name = builder.name;
        comment = builder.comment;
        ValidationUtil.validate(this);
    }

    public static class ItemDescriptionBuilder {

        public ItemDescription build() {
            return new ItemDescription(this);
        }

    }

}
