package com.fashionhouse.domain.warehouse.model;

public enum ParcelStatus {

    RETURNED,
    CANCELLED,
    AWAIT,
    PACKED,
    SENT,
    DELIVERED

}
