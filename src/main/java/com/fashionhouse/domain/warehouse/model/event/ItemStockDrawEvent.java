package com.fashionhouse.domain.warehouse.model.event;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("ItemStockDraw")
public class ItemStockDrawEvent extends ItemStockEvent {

    private Integer amount;

    public ItemStockDrawEvent(Integer amount) {
        this();
        this.amount = amount;
    }

    public int calculate() {
        return -amount;
    }

}
