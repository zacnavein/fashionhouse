package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import com.fashionhouse.infrastructure.validator.annotations.PositiveNumber;
import jakarta.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ValueObject
@ToString
@Getter
@Builder
@Embeddable
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Measurement {

    @PositiveNumber
    private Double length;
    @PositiveNumber
    private Double width;
    @PositiveNumber
    private Double height;
    @PositiveNumber
    private Double weight;

}
