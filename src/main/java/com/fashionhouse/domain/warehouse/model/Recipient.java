package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import com.fashionhouse.infrastructure.validator.ValidationUtil;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Recipient {

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String phone;
    private String email;

    private Recipient(Recipient.RecipientBuilder builder) {
        firstName = builder.firstName;
        lastName = builder.lastName;
        phone = builder.phone;
        email = builder.email;
        ValidationUtil.validate(this);
    }

    public static class RecipientBuilder {

        public Recipient build() {
            return new Recipient(this);
        }

    }

}
