package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.application.warehouse.command.ItemDescriptionCommand;
import com.fashionhouse.application.warehouse.command.ItemLocationCommand;
import com.fashionhouse.application.warehouse.command.ItemMeasurementCommand;
import org.springframework.stereotype.Component;

@Component
public class ItemFactory {

    public Measurement createMeasurement(ItemMeasurementCommand command) {
        return Measurement.builder()
                .length(command.getLength())
                .width(command.getWidth())
                .height(command.getHeight())
                .weight(command.getWeight())
                .build();
    }

    public Location createLocation(ItemLocationCommand command) {
        return Location.builder()
                .sector(command.getSector())
                .shelf(command.getShelf())
                .bucket(command.getBucket())
                .build();
    }

    public ItemDescription createItemDescription(ItemDescriptionCommand command) {
        return ItemDescription.builder()
                .name(command.getName())
                .comment(command.getComment())
                .build();
    }

}
