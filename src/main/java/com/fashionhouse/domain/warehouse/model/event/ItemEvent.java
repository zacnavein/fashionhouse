package com.fashionhouse.domain.warehouse.model.event;

import com.fashionhouse.infrastructure.util.TimeProvider;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import java.time.LocalDateTime;
import java.util.UUID;

@ToString
@Getter
@EqualsAndHashCode(of = "id")
@Entity(name = "item_events")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class ItemEvent {

    @Id
    @NotNull
    private UUID id;
    @NotNull
    private LocalDateTime date;

    ItemEvent() {
        this.id = UUID.randomUUID();
        this.date = TimeProvider.currentDateTime();
    }

    public static int sortNewest(ItemEvent event1, ItemEvent event2) {
        return event2.getDate().compareTo(event1.getDate());
    }

}
