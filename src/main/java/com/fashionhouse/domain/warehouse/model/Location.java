package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import com.fashionhouse.infrastructure.validator.ValidationUtil;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Location {

    @NotNull
    private String sector;
    @NotNull
    private String shelf;
    @NotNull
    private String bucket;

    private Location(Location.LocationBuilder builder) {
        sector = builder.sector;
        shelf = builder.shelf;
        bucket = builder.bucket;
        ValidationUtil.validate(this);
    }

    public static class LocationBuilder {

        public Location build() {
            return new Location(this);
        }

    }

}
