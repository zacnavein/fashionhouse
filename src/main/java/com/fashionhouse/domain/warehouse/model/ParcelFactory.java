package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.application.warehouse.command.*;
import com.fashionhouse.domain.warehouse.repository.ItemRepository;
import com.fashionhouse.infrastructure.annotations.Factory;
import com.fashionhouse.domain.warehouse.model.event.ParcelStatusEvent;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Factory
@AllArgsConstructor
public class ParcelFactory {

    private final ItemRepository itemRepository;

    public Parcel createParcel(CreateParcelDemandCommand command) {
        Map<UUID, Item> items = itemRepository.getItemsById(command.getItemIds());
        return Parcel.builder()
                .id(UUID.randomUUID())
                .orderId(command.getOrderId())
                .recipientId(command.getBuyerId())
                .recipient(command.getRecipient())
                .destination(command.getDestination())
                .parcelItems(createParcelItems(command, items))
                .events(Sets.newHashSet(new ParcelStatusEvent(ParcelStatus.AWAIT)))
                .build();
    }

    public Measurement createMeasurement(ParcelMeasurementCommand command) {
        return Measurement.builder()
                .length(command.getLength())
                .width(command.getWidth())
                .height(command.getHeight())
                .weight(command.getWeight())
                .build();
    }

    public Destination createDestination(ChangeParcelDeliveryCommand command) {
        return Destination.builder()
                .postcode(command.getPostcode())
                .city(command.getCity())
                .street(command.getStreet())
                .description(command.getDescription())
                .build();
    }

    public Invoice createInvoice(ChangeParcelInvoiceCommand command) {
        return Invoice.builder()
                .nip(command.getNip())
                .name(command.getName())
                .address(command.getAddress())
                .date(command.getDate())
                .build();
    }

    private Set<ParcelItem> createParcelItems(CreateParcelDemandCommand command, Map<UUID, Item> items) {
        return command.getParcelItems().stream()
                .map(parcelItem -> createParcelItem(parcelItem, items.get(parcelItem.getItemId())))
                .collect(Collectors.toSet());
    }

    private ParcelItem createParcelItem(ParcelItem parcelItem, Item item) {
        item.checkAvailability(parcelItem.getAmount());
        return ParcelItem.builder()
                .id(UUID.randomUUID())
                .itemId(parcelItem.getItemId())
                .item(item.getDescription())
                .amount(parcelItem.getAmount())
                .build();
    }

}
