package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.ValueObject;
import com.fashionhouse.infrastructure.validator.ValidationUtil;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.time.LocalDateTime;

@ValueObject
@Getter
@Builder
@ToString
@Embeddable
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Invoice {

    @NotNull
    private String name;
    private String nip;
    private String address;
    private LocalDateTime date;

    private Invoice(Invoice.InvoiceBuilder builder) {
        name = builder.name;
        nip = builder.nip;
        address = builder.address;
        date = builder.date;
        ValidationUtil.validate(this);
    }

    public static class InvoiceBuilder {

        public Invoice build() {
            return new Invoice(this);
        }

    }

}
