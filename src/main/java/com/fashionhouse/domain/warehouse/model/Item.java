package com.fashionhouse.domain.warehouse.model;

import com.fashionhouse.infrastructure.annotations.AggregateRoot;
import com.fashionhouse.domain.warehouse.ex.ItemOutOfStockException;
import com.fashionhouse.domain.warehouse.model.event.ItemStockDrawEvent;
import com.fashionhouse.domain.warehouse.model.event.ItemEvent;
import com.fashionhouse.domain.warehouse.model.event.ItemStockEvent;
import com.fashionhouse.domain.warehouse.model.event.ItemStockSupplyEvent;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import java.util.Set;
import java.util.UUID;

@AggregateRoot
@Entity
@Getter
@Builder
@ToString
@AllArgsConstructor
@Table(name = "items")
@EqualsAndHashCode(of = "id")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Item {

    @Id
    @NotNull
    private UUID id;
    @Embedded
    private ItemDescription description;
    @Embedded
    private Location location;
    @Embedded
    private Measurement measurement;
    @Getter(AccessLevel.NONE)
    @JoinColumn(name = "item_id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ItemEvent> events;

    public Integer getStock() {
        return events.stream()
                .filter(this::isStockEvent)
                .sorted(ItemEvent::sortNewest)
                .mapToInt(event -> ((ItemStockEvent) event).calculate())
                .sum();
    }

    private boolean isStockEvent(ItemEvent event) {
        return event instanceof ItemStockEvent;
    }

    public void changeLocation(Location location) {
        this.location = location;
    }

    public void changeDescription(ItemDescription description) {
        this.description = description;
    }

    public void changeMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public void supply(Integer amount) {
        events.add(new ItemStockSupplyEvent(amount));
    }

    public void draw(Integer amount) {
        checkAvailability(amount);
        events.add(new ItemStockDrawEvent(amount));
    }

    public void checkAvailability(Integer amount) {
        if (!isAvailable(amount)) {
            throw new ItemOutOfStockException(String.format("item '%s' is out of stock", id));
        }
    }

    public boolean isAvailable(Integer amount) {
        return getStock() >= amount;
    }

}
