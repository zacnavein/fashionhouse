package com.fashionhouse.domain.warehouse.model.event;

import com.fashionhouse.domain.warehouse.model.ParcelStatus;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString(callSuper = true)
@Entity
@Getter
@NoArgsConstructor
@DiscriminatorValue("ParcelStatus")
public class ParcelStatusEvent extends ParcelEvent {

    @Enumerated(EnumType.STRING)
    private ParcelStatus status;

    public ParcelStatusEvent(ParcelStatus status) {
        this();
        this.status = status;
    }

}
