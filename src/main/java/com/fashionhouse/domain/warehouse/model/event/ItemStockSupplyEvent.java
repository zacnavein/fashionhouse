package com.fashionhouse.domain.warehouse.model.event;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
@Entity
@NoArgsConstructor
@DiscriminatorValue("ItemStockSupply")
public class ItemStockSupplyEvent extends ItemStockEvent {

    private Integer amount;

    public ItemStockSupplyEvent(Integer amount) {
        this();
        this.amount = amount;
    }

    public int calculate() {
        return amount;
    }

}
