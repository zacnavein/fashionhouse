package com.fashionhouse.infrastructure.event.producer;

public interface EventProducer {

    void publishEvent(Object event);

}
