package com.fashionhouse.infrastructure.event.producer;

import com.google.common.eventbus.EventBus;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class GuavaEventProducer implements EventProducer {

    private final EventBus eventBus;

    /**
     * This is just a example
     * normally we should not implement this logic that way
     */
    @Override
    @Async
    @SneakyThrows
    public void publishEvent(Object event) {
        Thread.sleep(1000L);
        eventBus.post(event);
        log.debug("publish event : {}", event);
    }

}
