package com.fashionhouse.infrastructure.event.consumer;

import com.fashionhouse.application.sales.OrderStatusFacade;
import com.fashionhouse.domain.warehouse.event.ParcelPackedEvent;
import com.fashionhouse.domain.warehouse.event.ParcelReturnedEvent;
import com.fashionhouse.domain.warehouse.event.ParcelSentEvent;
import com.fashionhouse.infrastructure.event.producer.EventConsumer;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

@EventConsumer
@AllArgsConstructor
public class OrderEventConsumer {

    private final OrderStatusFacade orderStatusFacade;

    @Subscribe
    public void onParcelPackedEvent(ParcelPackedEvent event) {
        orderStatusFacade.markAsPacked(event.getOrderId());
    }

    @Subscribe
    public void onParcelSentEvent(ParcelSentEvent event) {
        orderStatusFacade.markAsSent(event.getOrderId());
    }

    @Subscribe
    public void onParcelReturnedEvent(ParcelReturnedEvent event) {
        orderStatusFacade.markAsReturned(event.getOrderId());
    }

}
