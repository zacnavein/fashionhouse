package com.fashionhouse.infrastructure.event.consumer;

import com.fashionhouse.application.warehouse.ParcelFacade;
import com.fashionhouse.application.warehouse.ParcelStatusFacade;
import com.fashionhouse.application.warehouse.command.ChangeParcelDeliveryCommand;
import com.fashionhouse.application.warehouse.command.ChangeParcelInvoiceCommand;
import com.fashionhouse.application.warehouse.command.CreateParcelDemandCommand;
import com.fashionhouse.domain.sales.event.*;
import com.fashionhouse.domain.warehouse.model.Destination;
import com.fashionhouse.domain.warehouse.model.Invoice;
import com.fashionhouse.domain.warehouse.model.ParcelItem;
import com.fashionhouse.domain.warehouse.model.Recipient;
import com.fashionhouse.infrastructure.authentication.UserProvider;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.event.producer.EventConsumer;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@EventConsumer
@AllArgsConstructor
public class ParcelEventConsumer {

    private final ParcelFacade parcelFacade;
    private final ParcelStatusFacade parcelStatusFacade;
    private final UserProvider userProvider;

    @Subscribe
    public void onRefundOrderEvent(RefundOrderEvent event) {
        User user = userProvider.getUserById(event.getRequesterId());
        parcelFacade.cancelParcelDelivery(user, event.getOrderId());
    }

    @Subscribe
    public void onSubmitOrderEvent(SubmitOrderEvent event) {
        parcelFacade.createParcelDemand(mapCommand(event));
    }

    @Subscribe
    public void onOrderDeliveryChangeEvent(OrderDeliveryChangeEvent event) {
        parcelFacade.changeParcelDelivery(
                ChangeParcelDeliveryCommand.builder()
                        .orderId(event.getOrderId())
                        .postcode(event.getPostcode())
                        .city(event.getCity())
                        .street(event.getStreet())
                        .description(event.getDescription())
                        .build());
    }

    @Subscribe
    public void onOrderInvoiceChangeEvent(OrderInvoiceChangeEvent event) {
        parcelFacade.changeParcelInvoice(
                ChangeParcelInvoiceCommand.builder()
                        .orderId(event.getOrderId())
                        .nip(event.getNip())
                        .name(event.getName())
                        .address(event.getAddress())
                        .date(event.getDate())
                        .build());
    }

    @Subscribe
    public void onDeliveredOrderEvent(DeliveredOrderEvent event) {
        User user = userProvider.getUserById(event.getRequesterId());
        parcelStatusFacade.deliveredParcel(user, event.getOrderId());
    }

    private CreateParcelDemandCommand mapCommand(SubmitOrderEvent event) {
        return CreateParcelDemandCommand.builder()
                .orderId(event.getOrderId())
                .buyerId(event.getBuyerId())
                .destination(mapDestination(event))
                .invoice(mapInvoice(event))
                .recipient(mapRecipient(event))
                .parcelItems(mapParcelItems(event))
                .build();
    }

    private Invoice mapInvoice(SubmitOrderEvent event) {
        return Invoice.builder()
                .address(event.getInvoice().getAddress())
                .nip(event.getInvoice().getNip())
                .name(event.getInvoice().getName())
                .date(event.getInvoice().getDate())
                .build();
    }

    private Recipient mapRecipient(SubmitOrderEvent event) {
        return com.fashionhouse.domain.warehouse.model.Recipient.builder()
                .email(event.getRecipient().getEmail())
                .firstName(event.getRecipient().getFirstName())
                .lastName(event.getRecipient().getFirstName())
                .phone(event.getRecipient().getPhone())
                .build();
    }

    private Destination mapDestination(SubmitOrderEvent event) {
        return Destination.builder()
                .postcode(event.getDelivery().getPostcode())
                .city(event.getDelivery().getCity())
                .street(event.getDelivery().getStreet())
                .description(event.getDelivery().getDescription())
                .build();
    }

    private List<ParcelItem> mapParcelItems(SubmitOrderEvent event) {
        return event.getOrderProducts().stream()
                .map(this::createParcelItem)
                .collect(Collectors.toList());
    }

    private ParcelItem createParcelItem(OrderProductDetails orderProduct) {
        return ParcelItem.builder()
                .itemId(orderProduct.getProductId())
                .amount(orderProduct.getAmount())
                .build();
    }

}
