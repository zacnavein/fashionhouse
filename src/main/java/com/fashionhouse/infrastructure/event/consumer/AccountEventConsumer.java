package com.fashionhouse.infrastructure.event.consumer;

import com.fashionhouse.domain.account.event.AccountCreatedEvent;
import com.fashionhouse.domain.account.event.AccountEmailChangedEvent;
import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.domain.account.repository.AccountRepository;
import com.fashionhouse.domain.account.service.AccountEmailService;
import com.fashionhouse.infrastructure.event.producer.EventConsumer;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

@EventConsumer
@AllArgsConstructor
public class AccountEventConsumer {

    private final AccountRepository accountRepository;
    private final AccountEmailService accountEmailService;

    @Subscribe
    public void onAccountCreatedEvent(AccountCreatedEvent event) {
        Account account = accountRepository.getById(event.getAccountId());
        accountEmailService.sendAccountCreatedEmail(account);
    }

    @Subscribe
    public void onAccountEmailChangedEvent(AccountEmailChangedEvent event) {
        Account account = accountRepository.getById(event.getAccountId());
        accountEmailService.sendPasswordChangedEmail(account);
    }

}
