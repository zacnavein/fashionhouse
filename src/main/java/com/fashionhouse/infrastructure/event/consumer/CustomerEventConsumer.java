package com.fashionhouse.infrastructure.event.consumer;

import com.fashionhouse.application.sales.CustomerFacade;
import com.fashionhouse.application.sales.command.ChangeCustomerEmailCommand;
import com.fashionhouse.application.sales.command.CreateCustomerCommand;
import com.fashionhouse.domain.account.event.AccountCreatedEvent;
import com.fashionhouse.domain.account.event.AccountEmailChangedEvent;
import com.fashionhouse.infrastructure.event.producer.EventConsumer;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

@EventConsumer
@AllArgsConstructor
public class CustomerEventConsumer {

    private final CustomerFacade customerFacade;

    @Subscribe
    public void onAccountCreatedEvent(AccountCreatedEvent event) {
        customerFacade.createCustomer(new CreateCustomerCommand(event.getAccountId(), event.getEmail()));
    }

    @Subscribe
    public void onAccountEmailChangedEvent(AccountEmailChangedEvent event) {
        customerFacade.changeCustomerEmail(new ChangeCustomerEmailCommand(event.getAccountId(), event.getEmail()));
    }

}
