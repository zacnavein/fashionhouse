package com.fashionhouse.infrastructure.event.consumer;

import com.fashionhouse.application.warehouse.ItemFacade;
import com.fashionhouse.application.warehouse.command.CreateItemCommand;
import com.fashionhouse.domain.design.event.DesignApprovedEvent;
import com.fashionhouse.infrastructure.event.producer.EventConsumer;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;

@EventConsumer
@AllArgsConstructor
public class ItemEventConsumer {

    private final ItemFacade itemFacade;

    @Subscribe
    public void onDesignApprovedEvent(DesignApprovedEvent event) {
        itemFacade.createItem(new CreateItemCommand(event.getDesignId(), event.getName(), event.getDescription()));
    }

}
