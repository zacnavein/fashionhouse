package com.fashionhouse.infrastructure.event.consumer;

import com.fashionhouse.application.sales.ProductManagementFacade;
import com.fashionhouse.application.sales.command.CreateProductCommand;
import com.fashionhouse.application.sales.command.ProductStockUpdateCommand;
import com.fashionhouse.domain.design.event.DesignApprovedEvent;
import com.fashionhouse.domain.warehouse.event.ItemDrawEvent;
import com.fashionhouse.domain.warehouse.event.ItemSupplyEvent;
import com.fashionhouse.infrastructure.event.producer.EventConsumer;
import com.google.common.eventbus.Subscribe;
import lombok.AllArgsConstructor;
import java.util.List;

@EventConsumer
@AllArgsConstructor
public class ProductEventConsumer {

    private final ProductManagementFacade productManagementFacade;

    @Subscribe
    public void onItemSupplMyEvent(ItemSupplyEvent event) {
        List<ProductStockUpdateCommand> commands = event.getSupplies().stream()
                .map(supply -> new ProductStockUpdateCommand(supply.getItemId(), supply.getStock()))
                .toList();
        productManagementFacade.updateProductStock(commands);
    }

    @Subscribe
    public void onItemDrawEvent(ItemDrawEvent event) {
        List<ProductStockUpdateCommand> commands = event.getDraws().stream()
                .map(supply -> new ProductStockUpdateCommand(supply.getItemId(), supply.getStock()))
                .toList();
        productManagementFacade.updateProductStock(commands);
    }

    @Subscribe
    public void onDesignApprovedEvent(DesignApprovedEvent event) {
        productManagementFacade.createProduct(
                CreateProductCommand.builder()
                        .productId(event.getDesignId())
                        .name(event.getName())
                        .description(event.getDescription())
                        .build()
        );
    }

}
