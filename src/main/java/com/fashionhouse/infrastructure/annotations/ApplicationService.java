package com.fashionhouse.infrastructure.annotations;

import com.fashionhouse.infrastructure.logger.Log;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Log
@Transactional(rollbackOn = Exception.class)
@Service
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationService {
}
