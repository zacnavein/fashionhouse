package com.fashionhouse.infrastructure.properties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class EmailSenderProperties {

    private String from;
    private String subject;
    private String body;

}
