package com.fashionhouse.infrastructure.authentication;

import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.authentication.ex.AuthenticationException;
import com.fashionhouse.infrastructure.authentication.factory.UserFactory;
import com.fashionhouse.infrastructure.authentication.factory.AuthenticationFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class JwtTokenAuthenticationProvider implements TokenAuthenticationProvider {

    private final JwtSecurityProperties jwtSecurityProperties;
    private final UserFactory accountDetailsFactory;
    private final AuthenticationFactory authenticationFactory;

    public Authentication authenticateToken(String token) {
        Claims claims = extractClaimsFromJwtToken(token);
        User user = accountDetailsFactory.createFromJwtTokenClaims(claims);
        return authenticationFactory.createAuthentication(user);
    }

    private Claims extractClaimsFromJwtToken(String jwtToken) {
        try {
            return Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecurityProperties.getSecret())))
                    .build()
                    .parseClaimsJws(jwtToken)
                    .getBody();
        } catch (JwtException e) {
            throw new AuthenticationException("invalid jwt token", e);
        }
    }

}
