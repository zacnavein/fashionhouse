package com.fashionhouse.infrastructure.authentication;

import org.springframework.security.core.Authentication;

public interface TokenAuthenticationProvider {

    Authentication authenticateToken(String token);

}
