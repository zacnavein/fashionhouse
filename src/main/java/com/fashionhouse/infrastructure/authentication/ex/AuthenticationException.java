package com.fashionhouse.infrastructure.authentication.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class AuthenticationException extends ClientSideException {

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
