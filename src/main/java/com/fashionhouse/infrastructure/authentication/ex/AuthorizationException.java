package com.fashionhouse.infrastructure.authentication.ex;

import com.fashionhouse.infrastructure.ex.ClientSideException;

public class AuthorizationException extends ClientSideException {

    public AuthorizationException(String message) {
        super(message);
    }

}
