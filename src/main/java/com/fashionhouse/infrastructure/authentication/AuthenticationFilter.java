package com.fashionhouse.infrastructure.authentication;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
public class AuthenticationFilter extends OncePerRequestFilter {

    private final JwtSecurityProperties jwtSecurityProperties;
    private final TokenAuthenticationProvider tokenAuthenticationProvider;

    @Override
    @SneakyThrows
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        authenticateRequest(request);
        chain.doFilter(request, response);
    }

    private void authenticateRequest(HttpServletRequest request) {
        try {
            getJwtTokenFromRequest(request).ifPresent(this::authenticate);
        } catch (AuthenticationException e) {
            log.warn("request not unauthorized", e);
        } catch (Exception e) {
            log.error("internal exception when authorizing request", e);
        }
    }

    private Optional<String> getJwtTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(jwtSecurityProperties.getHeader());
        return isValidBearerToken(bearerToken) ?
                Optional.of(extractJwtToken(bearerToken)):
                Optional.empty();
    }

    private String extractJwtToken(String bearerToken) {
        return bearerToken.substring(jwtSecurityProperties.getPrefix().length());
    }

    private boolean isValidBearerToken(String bearerToken) {
        return StringUtils.hasText(bearerToken) && bearerToken.startsWith(jwtSecurityProperties.getPrefix());
    }

    private void authenticate(String jwtToken) {
        SecurityContextHolder.getContext().setAuthentication(tokenAuthenticationProvider.authenticateToken(jwtToken));
    }

}
