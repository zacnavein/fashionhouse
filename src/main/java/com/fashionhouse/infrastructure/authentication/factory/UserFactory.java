package com.fashionhouse.infrastructure.authentication.factory;

import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.infrastructure.authentication.model.User;
import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.UUID;

@Component
@AllArgsConstructor
public class UserFactory {

    private final AuthorityFactory authorityFactory;

    public User createFromAccount(Account account) {
        return User.builder()
                .id(account.getId())
                .username(account.getEmail())
                .password(account.getPassword())
                .authorities(authorityFactory.createAuthorities(account))
                .build();
    }

    public User createFromJwtTokenClaims(Claims claims) {
        return User.builder()
                .id(UUID.fromString(claims.getId()))
                .username(claims.getSubject())
                .authorities(authorityFactory.createAuthorities(claims))
                .build();
    }

}
