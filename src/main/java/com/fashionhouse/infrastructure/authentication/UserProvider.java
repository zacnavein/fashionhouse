package com.fashionhouse.infrastructure.authentication;

import com.fashionhouse.domain.account.repository.AccountRepository;
import com.fashionhouse.infrastructure.authentication.factory.UserFactory;
import com.fashionhouse.infrastructure.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@AllArgsConstructor
public class UserProvider {

    private final AccountRepository accountRepository;
    private final UserFactory userFactory;

    public User getUserById(UUID userId) {
        return accountRepository.findById(userId)
                .map(userFactory::createFromAccount)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("user with id '%s' not found", userId)));
    }

}
