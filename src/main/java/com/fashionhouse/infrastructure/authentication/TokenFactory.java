package com.fashionhouse.infrastructure.authentication;

import com.fashionhouse.infrastructure.annotations.InfrastructureService;
import com.fashionhouse.infrastructure.authentication.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

@InfrastructureService
@AllArgsConstructor
public class TokenFactory {

    private final JwtSecurityProperties jwtSecurityProperties;

    public String createFromAuthentication(Authentication authentication) {
        LocalDateTime now = LocalDateTime.now();
        User user = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setId(user.getId().toString())
                .setSubject(user.getUsername())
                .setIssuedAt(toDate(now))
                .setExpiration(toDate(now.plusMinutes(jwtSecurityProperties.getValidMinutes())))
                .signWith(SignatureAlgorithm.HS512, jwtSecurityProperties.getSecret())
                .claim(tokenPermissionsKey(), user.getAuthorities())
                .compact();
    }

    public String tokenPermissionsKey() {
        return "permissions";
    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
