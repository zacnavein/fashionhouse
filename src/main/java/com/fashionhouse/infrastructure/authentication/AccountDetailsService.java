package com.fashionhouse.infrastructure.authentication;

import com.fashionhouse.domain.account.repository.AccountRepository;
import com.fashionhouse.infrastructure.annotations.InfrastructureService;
import com.fashionhouse.infrastructure.authentication.factory.UserFactory;
import com.fashionhouse.infrastructure.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@InfrastructureService
@AllArgsConstructor
public class AccountDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;
    private final UserFactory userFactory;

    @Override
    public User loadUserByUsername(String email) throws UsernameNotFoundException {
        return accountRepository.findByEmail(email)
                .map(userFactory::createFromAccount)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("user with email '%s' not found", email)));
    }

}
