package com.fashionhouse.infrastructure.ex;

public class ClientSideException extends RuntimeException {

    public ClientSideException() {
        super();
    }

    public ClientSideException(String message) {
        super(message);
    }

    public ClientSideException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ClientSideException(Throwable throwable) {
        super(throwable);
    }

}
