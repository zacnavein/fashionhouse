package com.fashionhouse.infrastructure.ex;

public class NotFoundException extends ClientSideException {

    public NotFoundException(String message) {
        super(message);
    }

}
