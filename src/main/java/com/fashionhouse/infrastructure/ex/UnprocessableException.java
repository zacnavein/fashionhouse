package com.fashionhouse.infrastructure.ex;

public class UnprocessableException extends ClientSideException {

    public UnprocessableException(String message) {
        super(message);
    }

}
