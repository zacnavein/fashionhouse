package com.fashionhouse.infrastructure.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

//    @Override
//    public void addFormatters(FormatterRegistry registry) {
//        DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
//        registrar.setUseIsoFormat(true);
//        registrar.registerFormatters(registry);
//    }
//
//    @Bean
//    public Docket api() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//                .apis(RequestHandlerSelectors.withClassAnnotation(Endpoint.class))
//                .paths(PathSelectors.any())
//                .build();
//    }
//
//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addRedirectViewController("/swagger/v2/api-docs", "/v2/api-docs");
//        registry.addRedirectViewController("/swagger/swagger-resources/configuration/ui","/swagger-resources/configuration/ui");
//        registry.addRedirectViewController("/swagger/swagger-resources/configuration/security","/swagger-resources/configuration/security");
//        registry.addRedirectViewController("/swagger/swagger-resources", "/swagger-resources");
//        registry.addRedirectViewController("/swagger", "/swagger/swagger-ui.html");
//    }
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/swagger/swagger-ui.html**").addResourceLocations("classpath:/META-INF/resources/");
//        registry.addResourceHandler("/swagger/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }

}
