package com.fashionhouse.infrastructure.config;

import com.fashionhouse.infrastructure.authentication.JwtSecurityProperties;
import com.fashionhouse.infrastructure.authentication.AuthenticationFilter;
import com.fashionhouse.infrastructure.authentication.TokenAuthenticationProvider;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig {

    private final JwtSecurityProperties jwtSecurityProperties;
    private final TokenAuthenticationProvider tokenAuthenticationProvider;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    protected SecurityFilterChain configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().headers().frameOptions().sameOrigin()
                .and().csrf().disable().cors()
                .and().exceptionHandling().authenticationEntryPoint((req, res, e) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED))
                .and().addFilterAfter(createAuthenticationFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests(requests -> requests
                        .requestMatchers(unauthenticatedAddresses()).permitAll()
                        .requestMatchers(unauthenticatedEndpoints()).permitAll()
                        .requestMatchers("/api/v1/accounts/authenticate").permitAll()
                        .requestMatchers("/api/v1/accounts/register").permitAll()
                    .anyRequest().authenticated());
        return http.build();
    }

    private String[] unauthenticatedAddresses() {
        return jwtSecurityProperties.getUnauthenticatedAddresses().toArray(new String[0]);
    }

    private String[] unauthenticatedEndpoints() {
        return jwtSecurityProperties.getUnauthenticatedEndpoints().stream()
                .map(this::api)
                .toArray(String[]::new);
    }

    private String api(String endpoint) {
        return "/api/v[0-9]" + endpoint;
    }

    private AuthenticationFilter createAuthenticationFilter() {
        return new AuthenticationFilter(jwtSecurityProperties, tokenAuthenticationProvider);
    }

}
