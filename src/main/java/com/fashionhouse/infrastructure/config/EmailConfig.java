package com.fashionhouse.infrastructure.config;

import com.fashionhouse.infrastructure.properties.EmailSenderProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailConfig {

    @Bean
    public EmailSenderProperties accountCreatedEmailProperties(
            @Value("${account-created.from}") String from,
            @Value("${account-created.subject}") String subject,
            @Value("${account-created.body}") String body) {
        return EmailSenderProperties.builder()
                .from(from)
                .subject(subject)
                .body(body)
                .build();
    }

    @Bean
    public EmailSenderProperties passwordChangedEmailProperties(
            @Value("${password-changed.from}") String from,
            @Value("${password-changed.subject}") String subject,
            @Value("${password-changed.body}") String body) {
        return EmailSenderProperties.builder()
                .from(from)
                .subject(subject)
                .body(body)
                .build();
    }

}
