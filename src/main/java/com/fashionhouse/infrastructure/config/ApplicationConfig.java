package com.fashionhouse.infrastructure.config;

import com.fashionhouse.infrastructure.util.FakeClock;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@EnableAsync
@Configuration
public class ApplicationConfig {

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return mapper;
    }

    @Bean
    public Executor asyncExecutor() {
        return Executors.newCachedThreadPool();
    }

    @Bean
    public EventBus eventBus(Executor asyncExecutor) {
        return new AsyncEventBus(asyncExecutor);
    }

    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }

    @Bean
    @Profile("test")
    public Clock fakeClock() {
        LocalDateTime LOCAL_DATE_TIME = LocalDateTime.parse("2020-01-01T08:00:00");
        ZoneOffset ZONE_OFFSET = ZoneOffset.UTC;
        return new FakeClock(LOCAL_DATE_TIME, ZONE_OFFSET);
    }

}
