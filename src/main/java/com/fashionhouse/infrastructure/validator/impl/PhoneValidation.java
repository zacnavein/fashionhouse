package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.Phone;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PhoneValidation implements ConstraintValidator<Phone, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.matches("\\d+");
    }

}
