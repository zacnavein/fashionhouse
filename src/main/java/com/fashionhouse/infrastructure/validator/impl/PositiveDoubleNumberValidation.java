package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.PositiveNumber;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PositiveDoubleNumberValidation implements ConstraintValidator<PositiveNumber, Double> {

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return value == null || value > 0;
    }

}
