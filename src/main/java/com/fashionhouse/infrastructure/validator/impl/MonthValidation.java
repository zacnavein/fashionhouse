package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.Month;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class MonthValidation implements ConstraintValidator<Month, Number> {

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return value == null || validate(value);
    }

    private boolean validate(Number value) {
        int val = value.intValue();
        return val >= 1 && val <= 12;
    }

}
