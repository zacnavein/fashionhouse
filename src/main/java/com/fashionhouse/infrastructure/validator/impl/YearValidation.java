package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.Year;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class YearValidation implements ConstraintValidator<Year, Number> {

    private static final Integer MIN = 1970;
    private static final Integer MAX = 2300;

    @Override
    public boolean isValid(Number value, ConstraintValidatorContext context) {
        return value == null || validate(value);
    }

    private boolean validate(Number value) {
        int val = value.intValue();
        return val >= MIN && val <= MAX;
    }

}
