package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.PositiveNumber;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PositiveNumberValidation implements ConstraintValidator<PositiveNumber, Long> {

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value == null || value > 0;
    }

}
