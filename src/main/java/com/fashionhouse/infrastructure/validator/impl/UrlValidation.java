package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.URL;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.apache.commons.validator.routines.UrlValidator;

public class UrlValidation implements ConstraintValidator<URL, String> {

    private static final UrlValidator urlValidator = new UrlValidator();

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || urlValidator.isValid(value);
    }

}
