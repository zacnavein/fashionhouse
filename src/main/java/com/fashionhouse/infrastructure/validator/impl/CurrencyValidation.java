package com.fashionhouse.infrastructure.validator.impl;

import com.fashionhouse.infrastructure.validator.annotations.Currency;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.math.BigDecimal;

public class CurrencyValidation implements ConstraintValidator<Currency, Double> {

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return value == null || validate(value);
    }

    private boolean validate(Double value) {
        BigDecimal noZero = BigDecimal.valueOf(value).stripTrailingZeros();
        int scale = Math.max(noZero.scale(), 0);
        return scale <= 2 && value.intValue() >= 0;
    }

}
