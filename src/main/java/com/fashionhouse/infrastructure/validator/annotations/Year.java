package com.fashionhouse.infrastructure.validator.annotations;

import com.fashionhouse.infrastructure.validator.impl.YearValidation;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = YearValidation.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Year {

    String message() default "Invalid year value";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
