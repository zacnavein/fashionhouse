package com.fashionhouse.infrastructure.validator.annotations;

import com.fashionhouse.infrastructure.validator.impl.MonthValidation;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = MonthValidation.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Month {

    String message() default "Invalid month value";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
