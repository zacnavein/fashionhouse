package com.fashionhouse.infrastructure.validator;

import com.fashionhouse.infrastructure.validator.ex.ValidationException;
import lombok.experimental.UtilityClass;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import java.util.stream.Collectors;

@UtilityClass
public final class ValidationUtil {

    public void validate(Object target) throws ValidationException {
        DataBinder binder = new DataBinder(target);
        binder.validate();
        BindingResult bindingResult = binder.getBindingResult();
        if (bindingResult.hasErrors()) {
            throw new ValidationException(String.format("'%s' validation exception: %s", target.getClass().getSimpleName(), createValidationMessage(bindingResult)), bindingResult);
        }
    }

    public String createValidationMessage(BindingResult bindingResult) {
        return "validation errors: " +
                bindingResult.getAllErrors().stream()
                        .map(ValidationUtil::createMessage)
                        .collect(Collectors.joining("; "));
    }

    private String createMessage(ObjectError error) {
        if (error instanceof FieldError) {
            FieldError fieldError = (FieldError) error;
            return "[" + fieldError.getField() + "] " + fieldError.getDefaultMessage();
        }
        return error.getDefaultMessage();
    }

}
