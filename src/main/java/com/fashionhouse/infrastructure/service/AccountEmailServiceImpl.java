package com.fashionhouse.infrastructure.service;

import com.fashionhouse.domain.account.model.Account;
import com.fashionhouse.domain.account.service.AccountEmailService;
import com.fashionhouse.infrastructure.properties.EmailSenderProperties;
import com.fashionhouse.infrastructure.rest.client.EmailServiceHttpClient;
import com.fashionhouse.infrastructure.rest.client.SendEmailRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AccountEmailServiceImpl implements AccountEmailService {

    private EmailServiceHttpClient emailService;
    private EmailSenderProperties accountCreatedEmailProperties;
    private EmailSenderProperties passwordChangedEmailProperties;

    @Override
    public void sendAccountCreatedEmail(Account account) {
        emailService.sendEmail(
                SendEmailRequest.builder()
                        .from(accountCreatedEmailProperties.getFrom())
                        .to(account.getEmail())
                        .subject(accountCreatedEmailProperties.getSubject())
                        .body(accountCreatedEmailProperties.getBody())
                        .build()
        );
    }

    @Override
    public void sendPasswordChangedEmail(Account account) {
        emailService.sendEmail(
                SendEmailRequest.builder()
                        .from(passwordChangedEmailProperties.getFrom())
                        .to(account.getEmail())
                        .subject(passwordChangedEmailProperties.getSubject())
                        .body(passwordChangedEmailProperties.getBody())
                        .build()
        );
    }

}
