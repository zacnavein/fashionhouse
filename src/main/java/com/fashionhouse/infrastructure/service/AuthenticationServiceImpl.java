package com.fashionhouse.infrastructure.service;

import com.fashionhouse.domain.account.service.AuthenticationService;
import com.fashionhouse.infrastructure.annotations.InfrastructureService;
import com.fashionhouse.infrastructure.authentication.TokenFactory;
import com.fashionhouse.infrastructure.authentication.ex.AuthenticationException;
import com.fashionhouse.infrastructure.authentication.factory.AuthenticationFactory;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

@InfrastructureService
@AllArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;
    private final AuthenticationFactory authenticationFactory;
    private final TokenFactory tokenFactory;

    @Override
    public String authenticate(String email, String password) {
        try {
            Authentication authentication = authenticationManager.authenticate(authenticationFactory.createAuthentication(email, password));
            return tokenFactory.createFromAuthentication(authentication);
        } catch (org.springframework.security.core.AuthenticationException e) {
            throw new AuthenticationException("invalid login or password", e);
        }
    }

}
