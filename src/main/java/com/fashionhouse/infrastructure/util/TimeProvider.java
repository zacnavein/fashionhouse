package com.fashionhouse.infrastructure.util;

import lombok.experimental.UtilityClass;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@UtilityClass
public class TimeProvider {

    public static final ZoneId ZONE = ZoneId.of("UTC");
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm z").withZone(ZONE);

    public Instant now() {
        return Instant.now(ClockProvider.provide());
    }

    public LocalDateTime currentDateTime() {
        return LocalDateTime.now(ClockProvider.provide());
    }

    public String format(LocalDateTime localDateTime) {
        return FORMATTER.format(localDateTime);
    }

}
