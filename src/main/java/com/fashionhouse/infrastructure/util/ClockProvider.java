package com.fashionhouse.infrastructure.util;

import java.time.Clock;

public class ClockProvider {

    static Clock clock;

    static {
        clock = ApplicationContextProvider.getApplicationContext().getBean(Clock.class);
    }

    public static Clock provide() {
        return clock;
    }

}
