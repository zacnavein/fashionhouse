package com.fashionhouse.infrastructure.util;

import java.time.*;

public class FakeClock extends Clock {

    private Instant zoneLocalDateTime;
    private LocalDateTime localDateTime;
    private ZoneOffset zone;
    private Instant timestamp;

    public FakeClock(LocalDateTime localDateTime, ZoneOffset zoneOffset){
        setupClock(localDateTime, zoneOffset);
    }

    public void setupClock(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        this.zone = zoneOffset;
        this.localDateTime = localDateTime;
        this.zoneLocalDateTime = localDateTime.toInstant(zoneOffset);
        this.timestamp = Instant.now();
    }

    @Override
    public ZoneId getZone() {
        return zone;
    }

    @Override
    public Clock withZone(ZoneId zone) {
        return new FakeClock(localDateTime, (ZoneOffset) zone);
    }

    @Override
    public Instant instant() {
        return nextInstant();
    }

    private Instant nextInstant() {
        return zoneLocalDateTime.plusMillis(Instant.now().toEpochMilli() - timestamp.toEpochMilli());
    }

}
