package com.fashionhouse.infrastructure.logger;

public enum LogLevel {

    DEBUG,
    INFO,
    WARN,
    ERROR

}
