package com.fashionhouse.infrastructure.logger;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
@AllArgsConstructor
public class LoggingAspect {

    private final LogTemplate logTemplate;

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {}

    @Pointcut("within(@com.fashionhouse.infrastructure.logger.Log *) || within(@(@com.fashionhouse.infrastructure.logger.Log *) *)")
    public void beanWithLogAnnotationOnClass() {}

    @Pointcut("@annotation(com.fashionhouse.infrastructure.logger.Log)")
    public void beanWithLogAnnotationOnMethod() {}

    @Around("beanWithLogAnnotationOnMethod() || (beanWithLogAnnotationOnClass() && publicMethod())")
    public Object logMethodExecution(ProceedingJoinPoint joinPoint) throws Throwable {
        LogModel logModel = LogModel.createFromProceedingJoinPoint(joinPoint);
        logMessage(logModel.getLogLevel(), logTemplate.createLogInMessage(logModel));
        logModel.setInTime(System.currentTimeMillis());
        Object result = joinPoint.proceed();
        logModel.setOutTime(System.currentTimeMillis());
        logModel.setResult(result);
        logMessage(logModel.getLogLevel(), logTemplate.createLogOutMessage(logModel));
        return result;
    }

    private void logMessage(LogLevel logLevel, String message) {
        switch (logLevel) {
            case DEBUG : log.debug(message); break;
            case INFO : log.info(message); break;
            case WARN : log.warn(message); break;
            case ERROR : log.error(message); break;
            default : throw new UnsupportedOperationException("unsupported logging aspect level : " + logLevel);
        }
    }

}
