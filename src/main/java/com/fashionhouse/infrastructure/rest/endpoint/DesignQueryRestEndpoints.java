package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.design.DesignQueryFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.DesignResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.DesignResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class DesignQueryRestEndpoints {

    private final DesignQueryFacade designQueryFacade;
    private final DesignResponseMapper mapper;

    @GetMapping("/designs/{designId}")
    public DesignResponse getDesign(@AuthenticationPrincipal User user, @PathVariable("designId") UUID designId) {
        return mapper.map(designQueryFacade.getDesign(user, designId));
    }

    @GetMapping("/designs")
    public List<DesignResponse> getDesigns(@AuthenticationPrincipal User user) {
        return mapper.map(designQueryFacade.getDesigns(user));
    }

}
