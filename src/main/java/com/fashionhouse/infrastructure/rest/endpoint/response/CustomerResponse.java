package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Builder
public class CustomerResponse {

    private UUID id;
    private String email;
    private String phone;
    private String firstName;
    private String lastName;
    private List<CustomerAddressResponse> addresses;

}
