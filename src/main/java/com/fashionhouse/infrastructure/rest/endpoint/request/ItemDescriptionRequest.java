package com.fashionhouse.infrastructure.rest.endpoint.request;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ItemDescriptionRequest {

    @NotNull
    private String name;
    private String comment;

}
