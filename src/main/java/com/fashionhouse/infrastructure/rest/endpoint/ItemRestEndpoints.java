package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.warehouse.ItemFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.WarehouseCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.ItemDescriptionRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ItemLocationRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ItemMeasurementRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ItemRestEndpoints {

    private final ItemFacade itemFacade;
    private final WarehouseCommandMapper mapper;

    @PutMapping("/items/{itemId}/description")
    public void updateItemDescription(
            @AuthenticationPrincipal User user,
            @PathVariable("itemId") UUID itemId,
            @Valid @RequestBody ItemDescriptionRequest request) {
        itemFacade.updateItemDescription(user, itemId, mapper.map(request));
    }

    @PutMapping("/items/{itemId}/location")
    public void updateItemLocation(
            @AuthenticationPrincipal User user,
            @PathVariable("itemId") UUID itemId,
            @Valid @RequestBody ItemLocationRequest request) {
        itemFacade.updateItemLocation(user, itemId, mapper.map(request));
    }

    @PutMapping("/items/{itemId}/measurement")
    public void updateItemMeasurement(
            @AuthenticationPrincipal User user,
            @PathVariable("itemId") UUID itemId,
            @Valid @RequestBody ItemMeasurementRequest request) {
        itemFacade.updateItemMeasurement(user, itemId, mapper.map(request));
    }

    @PutMapping("/items/{itemId}/supply/{amount}")
    public void itemSupply(
            @AuthenticationPrincipal User user,
            @PathVariable("itemId") UUID itemId,
            @PathVariable Integer amount) {
        itemFacade.itemSupply(user, itemId, amount);
    }

    @PutMapping("/items/{itemId}/draw/{amount}")
    public void itemDraw(
            @AuthenticationPrincipal User user,
            @PathVariable("itemId") UUID itemId,
            @PathVariable Integer amount) {
        itemFacade.itemDraw(user, itemId, amount);
    }

}
