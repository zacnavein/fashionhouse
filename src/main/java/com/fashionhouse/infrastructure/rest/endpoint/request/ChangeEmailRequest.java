package com.fashionhouse.infrastructure.rest.endpoint.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = "confirmingPassword")
@NoArgsConstructor
public class ChangeEmailRequest {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String confirmingPassword;

}
