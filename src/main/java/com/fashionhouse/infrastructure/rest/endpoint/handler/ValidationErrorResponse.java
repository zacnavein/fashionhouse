package com.fashionhouse.infrastructure.rest.endpoint.handler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ValidationErrorResponse extends StandardResponseError {

    private List<ValidationErrorField> validationErrors;

    public ValidationErrorResponse(String message, String requestId, List<ValidationErrorField> validationErrors) {
        super(null, message, requestId);
        this.validationErrors = validationErrors;
    }

}
