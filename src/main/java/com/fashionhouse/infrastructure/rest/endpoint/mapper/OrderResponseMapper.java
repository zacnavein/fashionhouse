package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.sales.query.*;
import com.fashionhouse.infrastructure.rest.endpoint.response.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class OrderResponseMapper {

    public List<OrderResponse> map(List<OrderView> orders) {
        return orders.stream().map(this::map).collect(Collectors.toList());
    }

    public OrderResponse map(OrderView order) {
        return OrderResponse.builder()
                .id(order.getId())
                .buyerId(order.getBuyerId())
                .delivery(map(order.getDelivery()))
                .recipient(map(order.getRecipient()))
                .invoice(map(order.getInvoice()))
                .products(order.getProducts().stream().map(this::map).collect(Collectors.toList()))
                .build();
    }

    private DeliveryResponse map(DeliveryView delivery) {
        return DeliveryResponse.builder()
                .city(delivery.getCity())
                .postcode(delivery.getPostcode())
                .street(delivery.getStreet())
                .description(delivery.getDescription())
                .build();
    }

    private RecipientResponse map(RecipientView recipient) {
        return RecipientResponse.builder()
                .firstName(recipient.getFirstName())
                .lastName(recipient.getLastName())
                .email(recipient.getEmail())
                .phone(recipient.getPhone())
                .build();
    }

    private InvoiceResponse map(InvoiceView invoice) {
        return InvoiceResponse.builder()
                .name(invoice.getName())
                .nip(invoice.getNip())
                .address(invoice.getAddress())
                .date(invoice.getDate())
                .build();
    }

    private OrderProductResponse map(OrderProductView product) {
        return OrderProductResponse.builder()
                .productId(product.getProductId())
                .amount(product.getAmount())
                .price(product.getPrice())
                .name(product.getName())
                .description(product.getDescription())
                .build();
    }

}
