package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class ParcelItemResponse {

    private UUID id;
    private UUID itemId;
    private Integer amount;
    private String name;

}
