package com.fashionhouse.infrastructure.rest.endpoint.request;

import com.fashionhouse.infrastructure.validator.annotations.PositiveNumber;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ItemMeasurementRequest {

    @NotNull
    @PositiveNumber
    private Double length;
    @NotNull
    @PositiveNumber
    private Double width;
    @NotNull
    @PositiveNumber
    private Double height;
    @NotNull
    @PositiveNumber
    private Double weight;

}
