package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryResponse {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
