package com.fashionhouse.infrastructure.rest.endpoint.request;

import com.fashionhouse.infrastructure.validator.annotations.URL;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DesignRequest {

    @NotNull
    private String name;
    @NotNull
    private String type;
    @NotNull
    private String description;
    @URL
    @NotNull
    private String url;

}
