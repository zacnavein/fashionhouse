package com.fashionhouse.infrastructure.rest.endpoint.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangeInvoiceRequest {

    private String name;
    private String nip;
    private String address;

}
