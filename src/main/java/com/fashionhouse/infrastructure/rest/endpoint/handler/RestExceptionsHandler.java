package com.fashionhouse.infrastructure.rest.endpoint.handler;

import com.fashionhouse.infrastructure.authentication.ex.AuthorizationException;
import com.fashionhouse.infrastructure.ex.ClientSideException;
import com.fashionhouse.infrastructure.ex.NotFoundException;
import com.fashionhouse.infrastructure.ex.UnprocessableException;
import com.fashionhouse.infrastructure.logger.MdcListener;
import jakarta.validation.ValidationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class RestExceptionsHandler {

    @ResponseBody
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<StandardResponseError> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(BindException.class)
    public ResponseEntity<ValidationErrorResponse> handleMethodArgumentNotValidException(BindException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage(), createValidationErrorFields(e.getBindingResult()));
    }

    @ResponseBody
    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<StandardResponseError> handleBindException(ValidationException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler({
            MissingServletRequestPartException.class,
            HttpRequestMethodNotSupportedException.class,
            MethodArgumentTypeMismatchException.class,
            ServletRequestBindingException.class,
            HttpMessageNotReadableException.class})
    public ResponseEntity<StandardResponseError> handleBadRequestException(Exception e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<StandardResponseError> handleNotFoundException(NotFoundException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler({AccessDeniedException.class, AuthorizationException.class})
    public ResponseEntity<StandardResponseError> handleAuthorizeException(RuntimeException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.FORBIDDEN, e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UnprocessableException.class)
    public ResponseEntity<StandardResponseError> handleNotFoundException(UnprocessableException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    /**
     * all client side exceptions that are not handled individually are treated as client side bad request
     */
    @ResponseBody
    @ExceptionHandler(ClientSideException.class)
    public ResponseEntity<StandardResponseError> handleClientSideException(ClientSideException e) {
        log.debug(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
    }

    /**
     * all exceptions that are not handled individually are treated as internal/server side exceptions
     */
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseEntity<StandardResponseError> handleInternalException(Exception e) {
        log.error(e.getMessage(), e);
        return createHttpErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "internal exception");
    }

    private ResponseEntity<ValidationErrorResponse> createHttpErrorResponse(HttpStatus httpStatus, String message, List<ValidationErrorField> errors) {
        return new ResponseEntity<>(new ValidationErrorResponse(message, MdcListener.getLogReqIdKey(), errors), httpStatus);
    }

    private ResponseEntity<StandardResponseError> createHttpErrorResponse(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new StandardResponseError(null, message, MdcListener.getLogReqIdKey()), httpStatus);
    }

    private List<ValidationErrorField> createValidationErrorFields(BindingResult bindingResult) {
        return bindingResult.getFieldErrors().stream()
                .map(this::createErrorField)
                .collect(Collectors.toList());
    }

    private ValidationErrorField createErrorField(FieldError error) {
        return ValidationErrorField.builder()
                .field(error.getField())
                .value(error.getRejectedValue())
                .message(error.getDefaultMessage())
                .code(error.getCode())
                .build();
    }

}
