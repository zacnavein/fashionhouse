package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.design.DesignFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.DesignCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.DesignRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class DesignRestEndpoints {

    private final DesignFacade designFacade;
    private final DesignCommandMapper mapper;

    @PostMapping("/designs")
    public void createDesign(@AuthenticationPrincipal User user, @Valid @RequestBody DesignRequest request) {
        designFacade.createDesign(user, mapper.map(request));
    }

    @PutMapping("/designs/{designId}")
    public void updateDesign(
            @AuthenticationPrincipal User user,
            @PathVariable("designId") UUID designId,
            @Valid @RequestBody DesignRequest request) {
        designFacade.updateDesign(user, designId, mapper.map(request));
    }

    @PutMapping("/designs/{designId}/finish")
    public void finishDesign(@AuthenticationPrincipal User user, @PathVariable("designId") UUID designId) {
        designFacade.finishDesign(user, designId);
    }

    @PutMapping("/designs/{designId}/cancel")
    public void cancelDesign(@AuthenticationPrincipal User user, @PathVariable("designId") UUID designId) {
        designFacade.cancelDesign(user, designId);
    }

}
