package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecipientResponse {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
