package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.ProductQueryFacade;
import com.fashionhouse.domain.sales.model.ProductStatus;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.ProductResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.ProductResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ProductQueryRestEndpoints {

    private final ProductQueryFacade productFacade;
    private final ProductResponseMapper mapper;

    @GetMapping("/products")
    public List<ProductResponse> getProducts(@RequestParam(value = "status", required = false, defaultValue = "AVAILABLE") ProductStatus status) {
        return mapper.map(productFacade.getProducts(status));
    }

    @GetMapping("/products/{productId}")
    public ProductResponse getProduct(@PathVariable UUID productId) {
        return mapper.map(productFacade.getProduct(productId));
    }

}
