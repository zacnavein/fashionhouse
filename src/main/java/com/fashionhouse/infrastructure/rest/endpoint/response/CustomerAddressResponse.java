package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class CustomerAddressResponse {

    private UUID id;
    private String postcode;
    private String city;
    private String street;
    private String description;

}
