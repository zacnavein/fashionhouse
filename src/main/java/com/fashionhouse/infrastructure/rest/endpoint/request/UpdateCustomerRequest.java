package com.fashionhouse.infrastructure.rest.endpoint.request;

import lombok.Data;

@Data
public class UpdateCustomerRequest {

    private String email;
    private String phone;
    private String firstName;
    private String lastName;

}
