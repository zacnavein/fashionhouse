package com.fashionhouse.infrastructure.rest.endpoint.response;

import com.fashionhouse.infrastructure.authentication.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {

    private UUID id;
    private String email;
    private Set<Role> roles;

}
