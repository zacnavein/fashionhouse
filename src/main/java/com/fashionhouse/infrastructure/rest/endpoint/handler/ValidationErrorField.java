package com.fashionhouse.infrastructure.rest.endpoint.handler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidationErrorField {

    private String field;
    private Object value;
    private String message;
    private String code;

}
