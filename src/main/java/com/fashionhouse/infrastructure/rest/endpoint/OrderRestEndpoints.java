package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.OrderFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.OrderCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangeInvoiceRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangeOrderDeliveryRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.SubmitOrderRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class OrderRestEndpoints {

    private final OrderFacade orderFacade;
    private final OrderCommandMapper mapper;

    @PostMapping("/customers/me/orders")
    public void submitOrder(@AuthenticationPrincipal User user, @Valid @RequestBody SubmitOrderRequest request) {
        orderFacade.submitOrder(user, mapper.map(request));
    }

    @PutMapping("/customers/me/orders/{orderId}/finish")
    public void finishOrder(@AuthenticationPrincipal User user, @PathVariable("orderId") UUID orderId) {
        orderFacade.finishOrder(user, orderId);
    }

    @PutMapping("/customers/me/orders/{orderId}/refund")
    public void refundOrder(@AuthenticationPrincipal User user, @PathVariable("orderId") UUID orderId) {
        orderFacade.refundOrder(user, orderId);
    }

    @PutMapping("/customers/me/orders/{orderId}/delivery")
    public void changeOrderDelivery(
            @AuthenticationPrincipal User user,
            @PathVariable("orderId") UUID orderId,
            @Valid @RequestBody ChangeOrderDeliveryRequest request) {
        orderFacade.changeOrderDelivery(user, orderId, mapper.map(request));
    }

    @PutMapping("/customers/me/orders/{orderId}/invoice")
    public void changeOrderInvoice(
            @AuthenticationPrincipal User user,
            @PathVariable("orderId") UUID orderId,
            @Valid @RequestBody ChangeInvoiceRequest request) {
        orderFacade.changeOrderInvoice(user, orderId, mapper.map(request));
    }

}
