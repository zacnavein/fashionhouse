package com.fashionhouse.infrastructure.rest.endpoint.handler;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class StandardResponseError {

    // code should be used to handle api call and it is not http status
    private Integer code;
    // message is used only as information for api client
    private String message;
    // request id to help find problems in logs
    private String requestId;

}
