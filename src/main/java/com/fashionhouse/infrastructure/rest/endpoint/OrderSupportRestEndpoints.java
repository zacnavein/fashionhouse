package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.OrderFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class OrderSupportRestEndpoints {

    private final OrderFacade orderFacade;

    @PutMapping("/customers/{customerId}/orders/{orderId}/refund")
    public void refundOrder(
            @AuthenticationPrincipal User user,
            @PathVariable("customerId") UUID customerId,
            @PathVariable("orderId") UUID orderId) {
        orderFacade.refundOrder(user, customerId, orderId);
    }

}
