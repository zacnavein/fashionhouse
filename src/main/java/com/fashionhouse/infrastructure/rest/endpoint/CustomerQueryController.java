package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.CustomerQueryFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.CustomerResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.CustomerResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class CustomerQueryController {

    private final CustomerQueryFacade customerQueryFacade;
    private final CustomerResponseMapper mapper;

    @GetMapping("/customers/me")
    public CustomerResponse getMyCustomer(@AuthenticationPrincipal User user) {
        return mapper.map(customerQueryFacade.getMyCustomer(user));
    }

    @GetMapping("/customers/{customerId}")
    public CustomerResponse getCustomer(@AuthenticationPrincipal User user, @PathVariable("customerId") UUID customerId) {
        return mapper.map(customerQueryFacade.getCustomer(user, customerId));
    }

    @GetMapping("/customers")
    public List<CustomerResponse> getCustomers(@AuthenticationPrincipal User user) {
        return mapper.map(customerQueryFacade.getCustomers(user));
    }

}
