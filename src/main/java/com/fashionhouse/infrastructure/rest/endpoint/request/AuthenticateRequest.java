package com.fashionhouse.infrastructure.rest.endpoint.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = "password")
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticateRequest {

    @NotNull
    private String email;
    @NotNull
    private String password;

}
