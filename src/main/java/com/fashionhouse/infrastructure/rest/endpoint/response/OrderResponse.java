package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponse {

    private UUID id;
    private UUID buyerId;
    private DeliveryResponse delivery;
    private RecipientResponse recipient;
    private InvoiceResponse invoice;
    private List<OrderProductResponse> products;

}
