package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.sales.query.CustomerAddressView;
import com.fashionhouse.application.sales.query.CustomerView;
import com.fashionhouse.infrastructure.rest.endpoint.response.CustomerAddressResponse;
import com.fashionhouse.infrastructure.rest.endpoint.response.CustomerResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerResponseMapper {

    public List<CustomerResponse> map(List<CustomerView> customers) {
        return customers.stream().map(this::map).collect(Collectors.toList());
    }

    public CustomerResponse map(CustomerView customer) {
        return CustomerResponse.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .email(customer.getEmail())
                .phone(customer.getPhone())
                .addresses(customer.getAddresses().stream().map(this::map).collect(Collectors.toList()))
                .build();
    }

    private CustomerAddressResponse map(CustomerAddressView address) {
        return CustomerAddressResponse.builder()
                .postcode(address.getPostcode())
                .city(address.getCity())
                .street(address.getStreet())
                .description(address.getDescription())
                .build();
    }

}
