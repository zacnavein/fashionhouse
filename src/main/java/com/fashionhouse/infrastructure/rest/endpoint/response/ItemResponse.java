package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class ItemResponse {

    private UUID id;
    private String name;
    private String comment;
    private Integer stock;
    private ItemLocationResponse location;
    private ItemMeasurementResponse measurement;

}
