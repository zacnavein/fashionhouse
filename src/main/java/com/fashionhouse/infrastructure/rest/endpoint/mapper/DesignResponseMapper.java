package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.design.query.DesignView;
import com.fashionhouse.infrastructure.rest.endpoint.response.DesignResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DesignResponseMapper {

    public List<DesignResponse> map(List<DesignView> designs) {
        return designs.stream().map(this::map).collect(Collectors.toList());
    }

    public DesignResponse map(DesignView design) {
        return DesignResponse.builder()
                .id(design.getId())
                .name(design.getName())
                .status(design.getStatus())
                .description(design.getDescription())
                .build();
    }

}
