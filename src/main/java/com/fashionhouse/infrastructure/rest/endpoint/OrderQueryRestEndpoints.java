package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.OrderQueryFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.OrderResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.OrderResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class OrderQueryRestEndpoints {

    private final OrderQueryFacade orderFacade;
    private final OrderResponseMapper mapper;

    @GetMapping("customers/me/orders/{orderId}")
    public OrderResponse getMyOrder(@AuthenticationPrincipal User user, @PathVariable("orderId") UUID orderId) {
        return mapper.map(orderFacade.getMyOrder(user, orderId));
    }

    @GetMapping("customers/me/orders")
    public List<OrderResponse> getMyOrders(@AuthenticationPrincipal User user) {
        return mapper.map(orderFacade.getMyOrders(user));
    }

    @GetMapping("/orders/{orderId}")
    public OrderResponse getOrder(@AuthenticationPrincipal User user, @PathVariable("orderId") UUID orderId) {
        return mapper.map(orderFacade.getOrder(user, orderId));
    }

    @GetMapping("/orders")
    public List<OrderResponse> getOrders(@AuthenticationPrincipal User user) {
        return mapper.map(orderFacade.getOrders(user));
    }

}
