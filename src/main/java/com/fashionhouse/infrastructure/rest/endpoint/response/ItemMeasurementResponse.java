package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemMeasurementResponse {

    private Double length;
    private Double width;
    private Double height;
    private Double weight;

}
