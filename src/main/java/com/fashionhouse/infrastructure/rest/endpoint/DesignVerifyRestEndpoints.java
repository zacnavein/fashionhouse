package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.design.DesignVerifyFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class DesignVerifyRestEndpoints {

    private final DesignVerifyFacade designVerifyFacade;

    @PutMapping("/designs/{designId}/approve")
    public void approveDesign(@AuthenticationPrincipal User user, @PathVariable("designId") UUID designId) {
        designVerifyFacade.approveDesign(user, designId);
    }

    @PutMapping("/designs/{designId}/decline")
    public void declineDesign(@AuthenticationPrincipal User user, @PathVariable("designId") UUID designId) {
        designVerifyFacade.declineDesign(user, designId);
    }

}
