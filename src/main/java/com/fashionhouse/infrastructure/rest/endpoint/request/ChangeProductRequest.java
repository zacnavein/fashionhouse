package com.fashionhouse.infrastructure.rest.endpoint.request;

import com.fashionhouse.infrastructure.validator.annotations.Currency;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ChangeProductRequest {

    @NotNull
    @Currency
    private Double price;
    @NotNull
    private String name;
    private String description;

}
