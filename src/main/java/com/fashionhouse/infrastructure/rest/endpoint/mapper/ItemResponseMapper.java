package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.warehouse.query.ItemLocationView;
import com.fashionhouse.application.warehouse.query.ItemMeasurementView;
import com.fashionhouse.application.warehouse.query.ItemView;
import com.fashionhouse.infrastructure.rest.endpoint.response.ItemLocationResponse;
import com.fashionhouse.infrastructure.rest.endpoint.response.ItemMeasurementResponse;
import com.fashionhouse.infrastructure.rest.endpoint.response.ItemResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ItemResponseMapper {

    public List<ItemResponse> map(List<ItemView> items) {
        return items.stream().map(this::map).collect(Collectors.toList());
    }

    public ItemResponse map(ItemView item) {
        return ItemResponse.builder()
                .id(item.getId())
                .name(item.getName())
                .stock(item.getStock())
                .measurement(map(item.getMeasurement()))
                .location(map(item.getLocation()))
                .comment(item.getComment())
                .build();
    }

    private ItemMeasurementResponse map(ItemMeasurementView itemMeasurement) {
        return ItemMeasurementResponse.builder()
                .length(itemMeasurement.getLength())
                .width(itemMeasurement.getWidth())
                .height(itemMeasurement.getHeight())
                .weight(itemMeasurement.getWeight())
                .build();
    }

    private ItemLocationResponse map(ItemLocationView itemLocation) {
        return ItemLocationResponse.builder()
                .sector(itemLocation.getSector())
                .shelf(itemLocation.getShelf())
                .bucket(itemLocation.getBucket())
                .build();
    }

}
