package com.fashionhouse.infrastructure.rest.endpoint.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(exclude = {"confirmingPassword", "newPassword"})
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordRequest {

    @NotNull
    private String newPassword;
    @NotNull
    private String confirmingPassword;

}
