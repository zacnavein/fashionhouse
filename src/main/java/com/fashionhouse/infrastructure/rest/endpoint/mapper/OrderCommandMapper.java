package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.sales.command.*;
import com.fashionhouse.infrastructure.rest.endpoint.request.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderCommandMapper {

    public SubmitOrderCommand map(SubmitOrderRequest request) {
        return SubmitOrderCommand.builder()
                .recipient(map(request.getRecipient()))
                .delivery(map(request.getDelivery()))
                .products(map(request.getProducts()))
                .invoice(map(request.getInvoice()))
                .build();
    }

    public List<OrderProductCommand> map(List<OrderProductRequest> request) {
        return request.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public OrderProductCommand map(OrderProductRequest request) {
        return new OrderProductCommand(request.getProductId(), request.getAmount());
    }

    public OrderDeliveryCommand map(OrderDeliveryRequest request) {
        return OrderDeliveryCommand.builder()
                .city(request.getCity())
                .postcode(request.getPostcode())
                .street(request.getStreet())
                .description(request.getDescription())
                .build();
    }

    public ChangeOrderDeliveryCommand map(ChangeOrderDeliveryRequest request) {
        return ChangeOrderDeliveryCommand.builder()
                .city(request.getCity())
                .postcode(request.getPostcode())
                .street(request.getStreet())
                .description(request.getDescription())
                .build();
    }

    public InvoiceCommand map(InvoiceRequest request) {
        return InvoiceCommand.builder()
                .name(request.getName())
                .nip(request.getNip())
                .address(request.getAddress())
                .build();
    }

    public ChangeInvoiceCommand map(ChangeInvoiceRequest request) {
        return ChangeInvoiceCommand.builder()
                .name(request.getName())
                .nip(request.getNip())
                .address(request.getAddress())
                .build();
    }

    public ChangeProductCommand map(ChangeProductRequest request) {
        return ChangeProductCommand.builder()
                .name(request.getName())
                .price(request.getPrice())
                .description(request.getDescription())
                .build();
    }

    public RecipientCommand map(RecipientRequest request) {
        return RecipientCommand.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .phone(request.getPhone())
                .build();
    }

}
