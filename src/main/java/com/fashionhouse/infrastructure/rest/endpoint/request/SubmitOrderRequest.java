package com.fashionhouse.infrastructure.rest.endpoint.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SubmitOrderRequest {

    @NotNull
    @NotEmpty
    private List<OrderProductRequest> products;
    @NotNull
    private RecipientRequest recipient;
    @NotNull
    private OrderDeliveryRequest delivery;
    private InvoiceRequest invoice;

}
