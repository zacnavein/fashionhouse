package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.sales.command.ChangeInvoiceCommand;
import com.fashionhouse.application.sales.command.ChangeOrderDeliveryCommand;
import com.fashionhouse.application.sales.command.CustomerAddressCommand;
import com.fashionhouse.application.sales.command.UpdateCustomerCommand;
import com.fashionhouse.infrastructure.rest.endpoint.request.CustomerAddressRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.InvoiceRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.OrderDeliveryRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.UpdateCustomerRequest;
import org.springframework.stereotype.Component;

@Component
public class CustomerCommandMapper {

    public ChangeInvoiceCommand map(InvoiceRequest request) {
        return ChangeInvoiceCommand.builder()
                .name(request.getName())
                .nip(request.getNip())
                .address(request.getAddress())
                .build();
    }

    public UpdateCustomerCommand map(UpdateCustomerRequest request) {
        return UpdateCustomerCommand.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .email(request.getEmail())
                .phone(request.getPhone())
                .build();
    }

    public ChangeOrderDeliveryCommand map(OrderDeliveryRequest request) {
        return ChangeOrderDeliveryCommand.builder()
                .city(request.getCity())
                .postcode(request.getPostcode())
                .street(request.getStreet())
                .description(request.getDescription())
                .build();
    }

    public CustomerAddressCommand map(CustomerAddressRequest request) {
        return CustomerAddressCommand.builder()
                .city(request.getCity())
                .postcode(request.getPostcode())
                .street(request.getStreet())
                .description(request.getDescription())
                .build();
    }

}
