package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParcelRecipientResponse {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
