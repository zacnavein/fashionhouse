package com.fashionhouse.infrastructure.rest.endpoint.response;

import com.fashionhouse.domain.sales.model.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderEventResponse {

    private OrderStatus status;
    private LocalDateTime date;

}
