package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class ProductResponse {

    private UUID id;
    private Double price;
    private String name;
    private String description;
    private Integer stock;

}
