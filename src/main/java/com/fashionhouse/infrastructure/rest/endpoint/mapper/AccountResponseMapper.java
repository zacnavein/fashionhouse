package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.account.query.AccountView;
import com.fashionhouse.infrastructure.rest.endpoint.response.AccountResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AccountResponseMapper {

    public List<AccountResponse> map(List<AccountView> accounts) {
        return accounts.stream().map(this::map).collect(Collectors.toList());
    }

    public AccountResponse map(AccountView account) {
        return AccountResponse.builder()
                .id(account.getId())
                .email(account.getEmail())
                .roles(account.getRoles())
                .build();
    }

}
