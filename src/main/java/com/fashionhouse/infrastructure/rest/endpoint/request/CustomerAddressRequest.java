package com.fashionhouse.infrastructure.rest.endpoint.request;

import lombok.Data;

@Data
public class CustomerAddressRequest {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
