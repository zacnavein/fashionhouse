package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.account.AccountManagementFacade;
import com.fashionhouse.application.account.command.ChangeEmailCommand;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.Role;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.AccountCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangeEmailRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangePasswordRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class AccountManagementRestEndpoints {

    private final AccountManagementFacade accountManagementFacade;
    private final AccountCommandMapper mapper;

    @PutMapping("/accounts/me/change_email")
    public void changeMyEmail(
            @AuthenticationPrincipal User user,
            @Valid @RequestBody ChangeEmailCommand request) {
        accountManagementFacade.changeMyEmail(user, request);
    }

    @PutMapping("/accounts/me/change_password")
    public void changeMyPassword(
            @AuthenticationPrincipal User user,
            @Valid @RequestBody ChangePasswordRequest request) {
        accountManagementFacade.changeMyPassword(user, mapper.map(request));
    }

    @PutMapping("/accounts/{accountId}/change_email")
    public void changeEmail(
            @AuthenticationPrincipal User user,
            @PathVariable("accountId") UUID accountId,
            @Valid @RequestBody ChangeEmailRequest request) {
        accountManagementFacade.changeEmail(user, accountId, mapper.map(request));
    }

    @PutMapping("/accounts/{accountId}/change_password")
    public void changePassword(
            @AuthenticationPrincipal User user,
            @PathVariable("accountId") UUID accountId,
            @Valid @RequestBody ChangePasswordRequest request) {
        accountManagementFacade.changePassword(user, accountId, mapper.map(request));
    }

    @PutMapping("/accounts/{accountId}/roles/{role}")
    public void assignRole(
            @AuthenticationPrincipal User user,
            @PathVariable("accountId") UUID accountId,
            @PathVariable("role") Role role) {
        accountManagementFacade.assigneeRole(user, accountId, role);
    }

    @DeleteMapping("/accounts/{accountId}/roles/{role}")
    public void removeRole(
            @AuthenticationPrincipal User user,
            @PathVariable("accountId") UUID accountId,
            @PathVariable("role") Role role) {
        accountManagementFacade.removeRole(user, accountId, role);
    }

    @DeleteMapping("/accounts/{accountId}")
    public void deleteAccount(@AuthenticationPrincipal User user, @PathVariable("accountId") UUID accountId) {
        accountManagementFacade.deleteAccount(user, accountId);
    }

}
