package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.warehouse.query.*;
import com.fashionhouse.infrastructure.rest.endpoint.response.*;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ParcelResponseMapper {

    public List<ParcelResponse> map(List<ParcelView> parcels) {
        return parcels.stream().map(this::map).collect(Collectors.toList());
    }

    public ParcelResponse map(ParcelView parcel) {
        return ParcelResponse.builder()
                .id(parcel.getId())
                .orderId(parcel.getOrderId())
                .destination(map(parcel.getDestination()))
                .recipient(map(parcel.getRecipient()))
                .measurement(map(parcel.getMeasurement()))
                .items(mapItems(parcel.getItems()))
                .build();
    }

    private List<ParcelItemResponse> mapItems(List<ParcelItemView> items) {
        return items.stream().map(this::map).collect(Collectors.toList());
    }

    private ParcelItemResponse map(ParcelItemView item) {
        return ParcelItemResponse.builder()
                .id(item.getId())
                .itemId(item.getItemId())
                .name(item.getName())
                .amount(item.getAmount())
                .build();
    }

    private ParcelDestinationResponse map(ParcelDestinationView delivery) {
        return ParcelDestinationResponse.builder()
                .city(delivery.getCity())
                .postcode(delivery.getPostcode())
                .street(delivery.getStreet())
                .description(delivery.getDescription())
                .build();
    }

    private ParcelRecipientResponse map(ParcelRecipientView recipient) {
        return ParcelRecipientResponse.builder()
                .firstName(recipient.getFirstName())
                .lastName(recipient.getLastName())
                .email(recipient.getEmail())
                .phone(recipient.getPhone())
                .build();
    }

    private ParcelMeasurementResponse map(ParcelMeasurementView measurement) {
        return ParcelMeasurementResponse.builder()
                .length(measurement.getLength())
                .width(measurement.getWidth())
                .height(measurement.getHeight())
                .weight(measurement.getWeight())
                .build();
    }

}
