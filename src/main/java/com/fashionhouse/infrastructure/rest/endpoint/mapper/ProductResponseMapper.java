package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.sales.query.ProductView;
import com.fashionhouse.infrastructure.rest.endpoint.response.ProductResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductResponseMapper {

    public List<ProductResponse> map(List<ProductView> products) {
        return products.stream().map(this::map).collect(Collectors.toList());
    }

    public ProductResponse map(ProductView product) {
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .stock(product.getStock())
                .description(product.getDescription())
                .build();
    }

}
