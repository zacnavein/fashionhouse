package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.account.AccountQueryFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.AccountResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.AccountResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class AccountQueryRestEndpoints {

    private final AccountQueryFacade accountQueryFacade;
    private final AccountResponseMapper mapper;

    @GetMapping("/accounts/me")
    public AccountResponse getMyAccount(@AuthenticationPrincipal User user) {
        return mapper.map(accountQueryFacade.getMyAccount(user));
    }

    @GetMapping("/accounts/{accountId}")
    public AccountResponse getAccount(@AuthenticationPrincipal User user, @PathVariable("accountId") UUID accountId) {
        return mapper.map(accountQueryFacade.getAccount(user, accountId));
    }

    @GetMapping("/accounts")
    public List<AccountResponse> getAccounts(@AuthenticationPrincipal User user) {
        return mapper.map(accountQueryFacade.getAccounts(user));
    }

}
