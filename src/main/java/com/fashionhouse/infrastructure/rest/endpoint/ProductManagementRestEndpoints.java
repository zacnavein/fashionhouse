package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.ProductManagementFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.ProductCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangeProductRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ProductManagementRestEndpoints {

    private final ProductManagementFacade productManagementFacade;
    private final ProductCommandMapper mapper;

    @PutMapping("/products/{productId}")
    public void changeProduct(
            @AuthenticationPrincipal User user,
            @PathVariable UUID productId,
            @Valid @RequestBody ChangeProductRequest request) {
        productManagementFacade.changeProduct(user, productId, mapper.map(request));
    }

    @PutMapping("/products/{productId}/available")
    public void availableProduct(@AuthenticationPrincipal User user, @PathVariable UUID productId) {
        productManagementFacade.availableProduct(user, productId);
    }

    @PutMapping("/products/{productId}/unavailable")
    public void unavailableProduct(@AuthenticationPrincipal User user, @PathVariable UUID productId) {
        productManagementFacade.unavailableProduct(user, productId);
    }

}
