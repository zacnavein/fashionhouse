package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.warehouse.command.ItemDescriptionCommand;
import com.fashionhouse.application.warehouse.command.ItemLocationCommand;
import com.fashionhouse.application.warehouse.command.ItemMeasurementCommand;
import com.fashionhouse.application.warehouse.command.ParcelMeasurementCommand;
import com.fashionhouse.infrastructure.rest.endpoint.request.ItemDescriptionRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ItemLocationRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ItemMeasurementRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ParcelMeasurementRequest;
import org.springframework.stereotype.Component;

@Component
public class WarehouseCommandMapper {

    public ItemDescriptionCommand map(ItemDescriptionRequest request) {
        return new ItemDescriptionCommand(request.getName(), request.getComment());
    }

    public ItemLocationCommand map(ItemLocationRequest request) {
        return ItemLocationCommand.builder()
                .sector(request.getSector())
                .shelf(request.getShelf())
                .bucket(request.getBucket())
                .build();
    }

    public ItemMeasurementCommand map(ItemMeasurementRequest request) {
        return ItemMeasurementCommand.builder()
                .length(request.getLength())
                .width(request.getWidth())
                .height(request.getHeight())
                .weight(request.getWeight())
                .build();
    }

    public ParcelMeasurementCommand map(ParcelMeasurementRequest request) {
        return ParcelMeasurementCommand.builder()
                .length(request.getLength())
                .width(request.getWidth())
                .height(request.getHeight())
                .weight(request.getWeight())
                .build();
    }

}
