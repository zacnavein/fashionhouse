package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemLocationResponse {

    private String sector;
    private String shelf;
    private String bucket;

}
