package com.fashionhouse.infrastructure.rest.endpoint.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductRequest {

    private UUID productId;
    private Integer amount;

}
