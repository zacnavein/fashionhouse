package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.sales.command.ChangeProductCommand;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangeProductRequest;
import org.springframework.stereotype.Component;

@Component
public class ProductCommandMapper {

    public ChangeProductCommand map(ChangeProductRequest request) {
        return ChangeProductCommand.builder()
                .name(request.getName())
                .price(request.getPrice())
                .description(request.getDescription())
                .build();
    }

}
