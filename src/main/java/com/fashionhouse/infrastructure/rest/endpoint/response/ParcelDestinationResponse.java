package com.fashionhouse.infrastructure.rest.endpoint.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParcelDestinationResponse {

    private String postcode;
    private String city;
    private String street;
    private String description;

}
