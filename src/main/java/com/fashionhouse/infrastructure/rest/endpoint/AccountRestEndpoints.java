package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.account.AccountFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.AccountCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.AuthenticateRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.RegistrationRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@ApiV1
@Endpoint
@AllArgsConstructor
public class AccountRestEndpoints {

    private final AccountFacade accountFacade;
    private final AccountCommandMapper mapper;

    @PostMapping("/accounts/authenticate")
    public String authenticate(@Valid @RequestBody AuthenticateRequest request) {
        return accountFacade.authenticate(mapper.map(request));
    }

    @PostMapping("/accounts/register")
    public void register(@Valid @RequestBody RegistrationRequest request) {
        accountFacade.register(mapper.map(request));
    }

}
