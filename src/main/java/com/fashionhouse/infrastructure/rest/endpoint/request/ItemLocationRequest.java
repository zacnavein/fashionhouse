package com.fashionhouse.infrastructure.rest.endpoint.request;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemLocationRequest {

    @NotNull
    private String sector;
    @NotNull
    private String shelf;
    @NotNull
    private String bucket;

}
