package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.warehouse.ParcelStatusFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.WarehouseCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.ParcelMeasurementRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ParcelRestEndpoints {

    private final ParcelStatusFacade parcelFacade;
    private final WarehouseCommandMapper mapper;

    @PutMapping("/parcels/{parcelId}/packed")
    public void packedParcel(
            @AuthenticationPrincipal User user,
            @PathVariable("parcelId") UUID parcelId,
            @Valid @RequestBody ParcelMeasurementRequest request) {
        parcelFacade.packedParcel(user, parcelId, mapper.map(request));
    }

    @PutMapping("/parcels/{parcelId}/send")
    public void sendParcel(@AuthenticationPrincipal User user, @PathVariable("parcelId") UUID parcelId) {
        parcelFacade.sendParcel(user, parcelId);
    }

    @PutMapping("/parcels/{parcelId}/returned")
    public void returnedParcel(@AuthenticationPrincipal User user, @PathVariable("parcelId") UUID parcelId) {
        parcelFacade.returnedParcel(user, parcelId);
    }

}
