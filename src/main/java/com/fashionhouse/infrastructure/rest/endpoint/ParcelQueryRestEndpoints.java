package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.warehouse.ParcelQueryFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.ParcelResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.ParcelResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ParcelQueryRestEndpoints {

    private final ParcelQueryFacade parcelQueryFacade;
    private final ParcelResponseMapper mapper;

    @GetMapping("/parcels")
    public List<ParcelResponse> getParcels(@AuthenticationPrincipal User user) {
        return mapper.map(parcelQueryFacade.getParcels(user));
    }

    @GetMapping("/parcels/{parcelId}")
    public ParcelResponse getParcel(@AuthenticationPrincipal User user, @PathVariable("parcelId") UUID parcelId) {
        return mapper.map(parcelQueryFacade.getParcel(user, parcelId));
    }

}
