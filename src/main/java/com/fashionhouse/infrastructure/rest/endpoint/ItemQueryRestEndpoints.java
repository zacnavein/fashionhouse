package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.warehouse.ItemQueryFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.ItemResponseMapper;
import com.fashionhouse.infrastructure.rest.endpoint.response.ItemResponse;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class ItemQueryRestEndpoints {

    private final ItemQueryFacade itemQueryFacade;
    private final ItemResponseMapper mapper;

    @GetMapping("/items")
    public List<ItemResponse> getItems(@AuthenticationPrincipal User user) {
        return mapper.map(itemQueryFacade.getItems(user));
    }

    @GetMapping("/items/{itemId}")
    public ItemResponse getItem(@AuthenticationPrincipal User user, @PathVariable("itemId") UUID itemId) {
        return mapper.map(itemQueryFacade.getItem(user, itemId));
    }

}
