package com.fashionhouse.infrastructure.rest.endpoint;

import com.fashionhouse.application.sales.CustomerFacade;
import com.fashionhouse.infrastructure.annotations.ApiV1;
import com.fashionhouse.infrastructure.annotations.Endpoint;
import com.fashionhouse.infrastructure.authentication.model.User;
import com.fashionhouse.infrastructure.rest.endpoint.mapper.CustomerCommandMapper;
import com.fashionhouse.infrastructure.rest.endpoint.request.CustomerAddressRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.UpdateCustomerRequest;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@ApiV1
@Endpoint
@AllArgsConstructor
public class CustomerRestEndpoints {

    private final CustomerFacade customerFacade;
    private final CustomerCommandMapper mapper;

    @PutMapping("/customers/me")
    public void updateCustomer(
            @AuthenticationPrincipal User user,
            @Valid @RequestBody UpdateCustomerRequest request) {
        customerFacade.updateCustomer(user, mapper.map(request));
    }

    @PostMapping("/customers/me/address")
    public void createAddress(
            @AuthenticationPrincipal User user,
            @Valid @RequestBody CustomerAddressRequest request) {
        customerFacade.createAddress(user, mapper.map(request));
    }

    @DeleteMapping("/customers/me/address/{addressId}")
    public void removeAddress(@AuthenticationPrincipal User user, @PathVariable("addressId") UUID addressId) {
        customerFacade.removeAddress(user, addressId);
    }


}
