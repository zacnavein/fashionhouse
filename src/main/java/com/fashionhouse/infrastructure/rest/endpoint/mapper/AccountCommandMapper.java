package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.account.command.AuthenticateCommand;
import com.fashionhouse.application.account.command.ChangeEmailCommand;
import com.fashionhouse.application.account.command.ChangePasswordCommand;
import com.fashionhouse.application.account.command.RegistrationCommand;
import com.fashionhouse.infrastructure.rest.endpoint.request.AuthenticateRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangeEmailRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.ChangePasswordRequest;
import com.fashionhouse.infrastructure.rest.endpoint.request.RegistrationRequest;
import org.springframework.stereotype.Component;

@Component
public class AccountCommandMapper {

    public AuthenticateCommand map(AuthenticateRequest request) {
        return new AuthenticateCommand(request.getEmail(), request.getPassword());
    }

    public RegistrationCommand map(RegistrationRequest request) {
        return new RegistrationCommand(request.getEmail(), request.getPassword());
    }

    public ChangeEmailCommand map(ChangeEmailRequest request) {
        return new ChangeEmailCommand(request.getEmail(), request.getConfirmingPassword());
    }

    public ChangePasswordCommand map(ChangePasswordRequest request) {
        return new ChangePasswordCommand(request.getNewPassword(), request.getConfirmingPassword());
    }

}
