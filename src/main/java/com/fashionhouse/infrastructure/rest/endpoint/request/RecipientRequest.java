package com.fashionhouse.infrastructure.rest.endpoint.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecipientRequest {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

}
