package com.fashionhouse.infrastructure.rest.endpoint.mapper;

import com.fashionhouse.application.design.command.DesignCommand;
import com.fashionhouse.infrastructure.rest.endpoint.request.DesignRequest;
import org.springframework.stereotype.Component;

@Component
public class DesignCommandMapper {

    public DesignCommand map(DesignRequest request) {
        return DesignCommand.builder()
                .name(request.getName())
                .type(request.getType())
                .description(request.getDescription())
                .url(request.getUrl())
                .build();
    }

}
