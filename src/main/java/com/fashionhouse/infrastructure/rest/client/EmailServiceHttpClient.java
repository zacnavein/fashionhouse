package com.fashionhouse.infrastructure.rest.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EmailServiceHttpClient {

    public void sendEmail(SendEmailRequest request) {
        // any implementation of http client to any email service
        log.info("sending email : " + request);
    }

}
