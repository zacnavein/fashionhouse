package com.fashionhouse.infrastructure.rest.client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SendEmailRequest {

    private String from;
    private String to;
    private String subject;
    private String body;

}
